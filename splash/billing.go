package splash

import (
	"fmt"
	"os"
	"strconv"

	"strings"

	"gitlab.com/vanoapp/api"
	"gitlab.com/vanoapp/splash"
)

// BillingService is
type BillingService struct {
	Client              *splash.Client
	MerchantService     api.MerchantService
	ProfessionalService api.ProfessionalService
	CustomerService     api.CustomerService
	TransactionService  api.TransactionService
}

// OnboardMerchant is
func (s *BillingService) OnboardMerchant(m *api.Merchant, c *api.Company, professional *api.Professional) error {
	ein := m.EIN
	companyType := "2"
	legalName := c.Name
	if m.EIN == "" {
		ein = m.SSN
		companyType = "0"
		legalName = professional.FirstName + " " + professional.LastName
	}
	req := &splash.FullMerchantRequest{
		Entity: splash.EntityReq{
			Login:    splash.String(os.Getenv("SPLASH_LOGIN_ID")),
			Type:     splash.String(companyType),
			Name:     splash.String(legalName),
			Address1: splash.String(c.AddressLine1),
			Address2: splash.String(c.AddressLine2),
			City:     splash.String(c.City),
			State:    splash.String(c.State),
			Zip:      splash.String(strconv.Itoa(c.ZipCode)),
			Country:  splash.String("USA"),
			Phone:    splash.String(professional.PhoneNumber),
			Ein:      splash.String(ein),
			Email:    splash.String(professional.Email),
			Currency: splash.String("USD"),
			Website:  splash.String("http://vanoapp.com/"),
			Accounts: []*splash.AccountReq{
				&splash.AccountReq{
					Primary: splash.String("1"),
					Account: splash.BankAccountReq{
						Routing: splash.String(m.RoutingNumber),
						Number:  splash.String(m.AccountNumber),
						Method:  8,
					},
					Currency: splash.String("USD"),
				},
			},
		},
		Mcc:       splash.String("7230"),
		New:       splash.String("0"),
		Status:    splash.String("1"),
		TcVersion: splash.String("1"),
		Members: []splash.MemberReq{
			splash.MemberReq{
				First:     splash.String(professional.FirstName),
				Last:      splash.String(professional.LastName),
				DOB:       splash.String("19930724"),
				Email:     splash.String(professional.Email),
				Ownership: splash.String("10000"),
				Frozen:    0,
				Title:     splash.String("Owner"),
				Primary:   splash.String("1"),
				Address1:  splash.String(c.AddressLine1),
				Address2:  splash.String(c.AddressLine2),
				City:      splash.String(c.City),
				State:     splash.String(c.State),
				Zip:       splash.String(strconv.Itoa(c.ZipCode)),
				Country:   splash.String("USA"),
				Phone:     splash.String(professional.PhoneNumber),
			},
		},
	}

	if len(m.SSN) > 0 {
		req.Members[0].Ssn = splash.String(m.SSN)
	}

	resp, err := s.Client.CreateFullMerchant(req)
	if err != nil {
		return err
	}

	m.SplashID = resp.ID

	return nil
}

// Charge is
func (s *BillingService) Charge(t *api.Transaction) error {
	professional, err := s.ProfessionalService.GetProfessional(t.ProfessionalID)
	if err != nil {
		return err
	}

	var m *api.Merchant
	if professional.Independent {
		m, err = s.MerchantService.GetProfessionalMerchant(professional.ID)
		if err != nil {
			return err
		}
	} else {
		m, err = s.MerchantService.GetCompanyMerchant(professional.CompanyID)
		if err != nil {
			return err
		}
	}

	req := &splash.TransactionRequest{
		Merchant: m.SplashID,
		Total:    t.Total,
		Type:     1,
		Origin:   splash.Int(2),
		Token:    splash.String(t.Token),
	}

	if t.Card != nil {
		// Valid values are: '1': American Express '2': Visa '3': MasterCard '4': Diners Club '5': Discover '6': PayPal '7': Debit card '8': Checking account '9': Savings account '10': Corporate checking account and '11': Corporate savings account '12': Gift card '13': EBT '14':WIC.
		var method int
		switch t.Card.Method {
		case "amex":
			method = 1
			break
		case "visa":
			method = 2
			break
		case "mastercard":
			method = 3
			break
		case "discover":
			method = 5
			break
		}
		req.Payment = &splash.Payment{
			Number:     t.Card.Number,
			Expiration: t.Card.Month + t.Card.Year[:2],
			Cvv:        t.Card.CVV,
			Method:     splash.Int(method),
		}
		req.Token = nil
	}

	resp, err := s.Client.CreateTransaction(req)
	if err != nil {
		return err
	}

	t.SplashID = resp.ID

	return nil
}

// Refund is
func (s *BillingService) Refund(transactionID string) error {
	t, err := s.TransactionService.GetTransactionByID(transactionID)
	if err != nil {
		return err
	}

	p, err := s.ProfessionalService.GetProfessional(t.ProfessionalID)
	if err != nil {
		return err
	}

	var m *api.Merchant
	if p.Independent {
		m, err = s.MerchantService.GetProfessionalMerchant(p.ID)
		if err != nil {
			return err
		}
	} else {
		m, err = s.MerchantService.GetCompanyMerchant(p.CompanyID)
		if err != nil {
			return err
		}
	}

	splashTxn, err := s.Client.GetTransaction(t.SplashID)
	if err != nil {
		return err
	}

	txnType := 5
	if splashTxn.Captured == nil {
		txnType = 4
	}

	if _, err := s.Client.RevokeTransaction(splashTxn.ID, m.SplashID, txnType); err != nil {
		return err
	}

	return nil
}

// CreateMerchantCustomer is
func (s *BillingService) CreateMerchantCustomer(c *api.Customer) error {
	email := c.Email
	if c.Email == "" {
		email = "unsetemail@vano.com"
	}

	req := &splash.CustomerRequest{
		First: c.FirstName,
		Last:  c.LastName,
		Email: email,
	}

	resp, err := s.Client.CreateCustomer(req)
	if err != nil {
		return err
	}

	c.SplashID = resp.ID

	return nil
}

// CreateToken is
func (s *BillingService) CreateToken(c *api.CustomerCard) error {
	customer, err := s.CustomerService.GetCustomer(c.CustomerID)
	if err != nil {
		return err
	}

	if customer.SplashID == "" {
		if err := s.CreateMerchantCustomer(customer); err != nil {
			return err
		}
		if _, err := s.CustomerService.SetSplashID(customer.ID, customer.SplashID); err != nil {
			return err
		}
	}

	var method int
	switch c.Method {
	case "amex":
		method = 1
		break
	case "visa":
		method = 2
		break
	case "mastercard":
		method = 3
		break
	case "dinersclub":
		method = 4
		break
	case "discover":
		method = 5
		break
	default:
		method = 7
	}

	exp := strings.Split(c.Exp, "/")[0] + strings.Split(c.Exp, "/")[1][:2]

	req := &splash.TokenRequest{
		Customer: customer.SplashID,
		Payment: &splash.Payment{
			Method:     splash.Int(method),
			Number:     c.Number,
			Expiration: exp,
			Cvv:        c.CVV,
		},
	}

	resp, err := s.Client.CreateToken(req)
	if err != nil {
		return err
	}

	c.Token = resp.Token

	return nil
}

// GetCompanyTransactions is
func (s *BillingService) GetCompanyTransactions(companyID, date string) (interface{}, error) {
	m, err := s.MerchantService.GetCompanyMerchant(companyID)
	if err != nil {
		if err == api.ErrNotFound {
			return nil, api.ErrMerchantNotIntegrated
		}
		return nil, err
	}

	search := fmt.Sprintf("merchant[equals]=%s&created[Greater]=%s", m.SplashID, date)

	resp, err := s.Client.GetTransactions(search)
	if err != nil {
		return nil, err
	}

	return resp, nil
}
