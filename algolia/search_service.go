package algolia

import (
	"context"
	"encoding/json"
	"fmt"
	"os"

	"googlemaps.github.io/maps"

	"gitlab.com/vanoapp/api"

	"github.com/algolia/algoliasearch-client-go/algoliasearch"
)

// CompanyObj is
type CompanyObj struct {
	ObjectID        string             `json:"objectID"`
	Name            string             `json:"name"`
	Address         string             `json:"address"`
	GeoLoc          LatLong            `json:"_geoloc"`
	ProfessionalObj []*ProfessionalObj `json:"professionals"`
	CoverURL        string             `json:"cover_url"`
}

// ProfessionalObj is
type ProfessionalObj struct {
	ID        string        `json:"id"`
	FirstName string        `json:"first_name"`
	LastName  string        `json:"last_name"`
	Services  []*ServiceObj `json:"services"`
}

//ServiceObj is
type ServiceObj struct {
	ID    string `json:"id"`
	Name  string `json:"name"`
	Price int    `json:"price"`
}

//CustomerObj is
type CustomerObj struct {
	ObjectID      string   `json:"objectID"`
	Companies     []string `json:"companies"`
	FirstName     string   `json:"first_name"`
	LastName      string   `json:"last_name"`
	Email         string   `json:"email"`
	PhoneNumber   string   `json:"phone_number"`
	Professionals []string `json:"professionals"`
}

// LatLong is
type LatLong struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

// SearchService is
type SearchService struct {
	AlgoliaClient       algoliasearch.Client
	ProfessionalService api.ProfessionalService
}

// AddCompany is
func (s *SearchService) AddCompany(c *api.SearchCompany) error {
	index := s.AlgoliaClient.InitIndex(os.Getenv("ALGOLIA_SALON_INDEX"))

	googleClient, err := maps.NewClient(maps.WithAPIKey(os.Getenv("GOOGLE_MAPS_API_KEY")))
	if err != nil {
		return err
	}

	req := &maps.GeocodingRequest{
		Address: c.Address,
	}

	resp, err := googleClient.Geocode(context.Background(), req)
	if err != nil {
		return err
	}

	location := resp[0].Geometry.Location

	companyObj := &CompanyObj{
		ObjectID:        c.ID,
		Name:            c.Name,
		Address:         c.Address,
		ProfessionalObj: []*ProfessionalObj{},
		CoverURL:        c.CoverURL,
		GeoLoc: LatLong{
			Lat: location.Lat,
			Lng: location.Lng,
		},
	}

	b, err := json.Marshal(companyObj)
	if err != nil {
		return err
	}

	var object algoliasearch.Object
	if err := json.Unmarshal(b, &object); err != nil {
		return err
	}

	if _, err := index.AddObject(object); err != nil {
		return err
	}

	return nil
}

// UpdateCompany is
func (s *SearchService) UpdateCompany(c *api.SearchCompany) error {
	index := s.AlgoliaClient.InitIndex(os.Getenv("ALGOLIA_SALON_INDEX"))

	// Geocode address
	googleClient, err := maps.NewClient(maps.WithAPIKey(os.Getenv("GOOGLE_MAPS_API_KEY")))
	if err != nil {
		return fmt.Errorf("error creating google client %v", err)
	}
	req := &maps.GeocodingRequest{
		Address: c.Address,
	}
	resp, err := googleClient.Geocode(context.Background(), req)
	if err != nil {
		return fmt.Errorf("error deociding address, %s, %v", c.Address, err)
	}
	location := resp[0].Geometry.Location

	// Get sps
	profs, err := s.ProfessionalService.ListCompanyProfessionals(c.ID)
	if err != nil {
		return fmt.Errorf("error getting sps %v", err)
	}
	var sps []*ProfessionalObj
	for _, p := range profs {
		var svcs []*ServiceObj
		for _, s := range p.Services {
			svc := &ServiceObj{
				ID:    s.ID,
				Name:  s.Name,
				Price: s.Price,
			}
			svcs = append(svcs, svc)
		}

		sp := &ProfessionalObj{
			ID:        p.ID,
			FirstName: p.FirstName,
			LastName:  p.LastName,
			Services:  svcs,
		}
		sps = append(sps, sp)
	}
	companyObj := &CompanyObj{
		ObjectID: c.ID,
		Name:     c.Name,
		CoverURL: c.CoverURL,
		Address:  c.Address,
		GeoLoc: LatLong{
			Lat: location.Lat,
			Lng: location.Lng,
		},
		ProfessionalObj: sps,
	}

	b, err := json.Marshal(companyObj)
	if err != nil {
		return err
	}

	var object algoliasearch.Object
	if err := json.Unmarshal(b, &object); err != nil {
		return err
	}

	if _, err := index.UpdateObject(object); err != nil {
		return err
	}

	return nil
}

// AddProfessional is
func (s *SearchService) AddProfessional(p *api.SearchProfessional) error {
	index := s.AlgoliaClient.InitIndex(os.Getenv("ALGOLIA_SALON_INDEX"))

	resp, err := index.GetObject(p.CompanyID, nil)
	if err != nil {
		return err
	}

	b, err := json.Marshal(resp)
	if err != nil {
		return err
	}

	var companyObj CompanyObj
	if err := json.Unmarshal(b, &companyObj); err != nil {
		return err
	}

	var svs []*ServiceObj
	for _, service := range p.Services {
		svc := &ServiceObj{
			ID:    service.ID,
			Name:  service.Name,
			Price: service.Price,
		}

		svs = append(svs, svc)
	}

	profObj := &ProfessionalObj{
		ID:        p.ID,
		FirstName: p.FirstName,
		LastName:  p.LastName,
		Services:  svs,
	}

	companyObj.ProfessionalObj = append(companyObj.ProfessionalObj, profObj)

	pb, err := json.Marshal(companyObj)
	if err != nil {
		return err
	}

	var object algoliasearch.Object
	if err := json.Unmarshal(pb, &object); err != nil {
		return err
	}

	if _, err := index.UpdateObject(object); err != nil {
		return err
	}

	return nil
}

//AddCustomer is
func (s *SearchService) AddCustomer(c *api.SearchCustomer) error {
	index := s.AlgoliaClient.InitIndex(os.Getenv("ALGOLIA_CUSTOMER_INDEX"))

	var companies []string
	if c.CompanyID != "" {
		companies = append(companies, c.CompanyID)
	}

	var professionals []string
	if c.ProfessionalID != "" {
		professionals = append(professionals, c.ProfessionalID)
	}

	companyObj := &CustomerObj{
		ObjectID:      c.ID,
		Companies:     companies,
		FirstName:     c.FirstName,
		LastName:      c.LastName,
		Email:         c.Email,
		PhoneNumber:   c.PhoneNumber,
		Professionals: professionals,
	}

	b, err := json.Marshal(companyObj)
	if err != nil {
		return err
	}

	var object algoliasearch.Object
	if err := json.Unmarshal(b, &object); err != nil {
		return err
	}

	if _, err := index.AddObject(object); err != nil {
		return err
	}

	return nil
}

//UpdateCustomer is
func (s *SearchService) UpdateCustomer(c *api.SearchCustomer) error {
	index := s.AlgoliaClient.InitIndex(os.Getenv("ALGOLIA_CUSTOMER_INDEX"))

	resp, err := index.GetObject(c.ID, nil)
	if err != nil {
		return err
	}

	b, err := json.Marshal(resp)
	if err != nil {
		return err
	}

	var algCustObj CustomerObj
	if err := json.Unmarshal(b, &algCustObj); err != nil {
		return err
	}

	companies := algCustObj.Companies
	professionals := algCustObj.Professionals

	customerObj := &CustomerObj{
		ObjectID:      c.ID,
		Companies:     companies,
		FirstName:     c.FirstName,
		LastName:      c.LastName,
		Email:         c.Email,
		PhoneNumber:   c.PhoneNumber,
		Professionals: professionals,
	}

	b, err = json.Marshal(customerObj)
	if err != nil {
		return err
	}

	var object algoliasearch.Object
	if err := json.Unmarshal(b, &object); err != nil {
		return err
	}

	if _, err := index.UpdateObject(object); err != nil {
		return err
	}

	return nil
}

//UpdateProfessionalServices is
func (s *SearchService) UpdateProfessionalServices(id string) error {
	index := s.AlgoliaClient.InitIndex(os.Getenv("ALGOLIA_SALON_INDEX"))

	p, err := s.ProfessionalService.GetProfessional(id)
	if err != nil {
		return err
	}

	resp, err := index.GetObject(p.CompanyID, nil)
	if err != nil {
		return err
	}

	by, err := json.Marshal(resp)
	if err != nil {
		return err
	}

	var companyObj CompanyObj
	if err := json.Unmarshal(by, &companyObj); err != nil {
		return err
	}

	// Get sps
	profs, err := s.ProfessionalService.ListCompanyProfessionals(p.CompanyID)
	if err != nil {
		return fmt.Errorf("error getting sps %v", err)
	}
	var sps []*ProfessionalObj
	for _, p := range profs {
		var svcs []*ServiceObj
		for _, s := range p.Services {
			svc := &ServiceObj{
				ID:    s.ID,
				Name:  s.Name,
				Price: s.Price,
			}
			svcs = append(svcs, svc)
		}

		sp := &ProfessionalObj{
			ID:        p.ID,
			FirstName: p.FirstName,
			LastName:  p.LastName,
			Services:  svcs,
		}
		sps = append(sps, sp)
	}

	companyObj.ProfessionalObj = sps

	b, err := json.Marshal(companyObj)
	if err != nil {
		return err
	}

	var object algoliasearch.Object
	if err := json.Unmarshal(b, &object); err != nil {
		return err
	}

	if _, err := index.UpdateObject(object); err != nil {
		return err
	}

	return nil
}

//AddCompanyCustomer is
func (s *SearchService) AddCompanyCustomer(customerID, companyID string) error {
	index := s.AlgoliaClient.InitIndex(os.Getenv("ALGOLIA_CUSTOMER_INDEX"))

	resp, err := index.GetObject(customerID, nil)
	if err != nil {
		return err
	}

	b, err := json.Marshal(resp)
	if err != nil {
		return err
	}

	var customerObj CustomerObj
	if err := json.Unmarshal(b, &customerObj); err != nil {
		return err
	}

	companies := customerObj.Companies
	companies = append(companies, companyID)

	customerObj.Companies = companies

	b, err = json.Marshal(customerObj)
	if err != nil {
		return err
	}

	var object algoliasearch.Object
	if err := json.Unmarshal(b, &object); err != nil {
		return err
	}

	if _, err := index.PartialUpdateObject(object); err != nil {
		return err
	}

	return nil
}

//RemoveCompanyCustomer is
func (s *SearchService) RemoveCompanyCustomer(customerID, companyID string) error {
	index := s.AlgoliaClient.InitIndex(os.Getenv("ALGOLIA_CUSTOMER_INDEX"))

	resp, err := index.GetObject(customerID, nil)
	if err != nil {
		return err
	}

	b, err := json.Marshal(resp)
	if err != nil {
		return err
	}

	var customerObj CustomerObj
	if err := json.Unmarshal(b, &customerObj); err != nil {
		return err
	}

	companies := customerObj.Companies
	companies = removeString(companies, companyID)

	customerObj.Companies = companies

	b, err = json.Marshal(customerObj)
	if err != nil {
		return err
	}

	var object algoliasearch.Object
	if err := json.Unmarshal(b, &object); err != nil {
		return err
	}

	if _, err := index.PartialUpdateObject(object); err != nil {
		return err
	}

	return nil
}

func removeString(s []string, r string) []string {
	for i, v := range s {
		if v == r {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}
