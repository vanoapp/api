package postgre

import (
	"fmt"

	"gitlab.com/vanoapp/api/hum"
	pg "gopkg.in/pg.v5"
)

// Migrater is
type Migrater struct {
	DB         *pg.DB
	Migrations []*hum.Migration
	LastBatch  int
}

// NewMigrater is
func NewMigrater(db *pg.DB) *Migrater {
	return &Migrater{
		DB: db,
	}
}

// Migrate is
func (m *Migrater) Migrate() error {
	if err := m.createMigrationTable(); err != nil {
		return err
	}

	var dbMigrations []*hum.MigrationTable
	if err := m.DB.Model(&dbMigrations).Select(); err != nil {
		return err
	}

	var names []string
	for _, dbmi := range dbMigrations {
		names = append(names, dbmi.Name)
		if dbmi.Batch > m.LastBatch {
			m.LastBatch = dbmi.Batch
		}
	}

	for _, mi := range m.Migrations {
		if !hum.StringInSlice(names, mi.Name) {
			if err := m.migrate(mi); err != nil {
				return err
			}
			fmt.Println("done creating", mi.Name)
		}
	}

	return nil
}

// RollBack is
func (m *Migrater) RollBack() error {
	return nil
}

// AppendMigration is
func (m *Migrater) AppendMigration(mi *hum.Migration) {
	m.Migrations = append(m.Migrations, mi)
}

func (m *Migrater) migrate(mi *hum.Migration) error {
	if _, err := m.DB.Exec(mi.Up); err != nil {
		return err
	}

	t := hum.MigrationTable{
		Name:  mi.Name,
		Batch: m.LastBatch + 1,
	}

	if err := m.DB.Insert(&t); err != nil {
		return err
	}

	return nil
}

func (m *Migrater) createMigrationTable() error {
	sql := `
		CREATE TABLE IF NOT EXISTS migrations (
			"id" serial,
			"name" text NOT NULL,
			"batch" int NOT NULL,
			PRIMARY KEY ("id")
		)
	`

	if _, err := m.DB.Exec(sql); err != nil {
		return err
	}

	return nil
}

func (m *Migrater) insertMigration(mi *hum.Migration) error {
	t := hum.MigrationTable{
		Name:  mi.Name,
		Batch: m.LastBatch + 1,
	}

	return m.DB.Insert(&t)
}
