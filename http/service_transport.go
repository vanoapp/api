package http

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/vanoapp/api"
)

func (h *Handler) handleCreateService() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload api.CreateServiceRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := payload.Validate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		var steps []*api.Service
		if len(payload.Steps) > 0 {
			for _, s := range payload.Steps {
				step := &api.Service{
					Name:   s.Name,
					Length: s.Length,
					IsGap:  s.IsGap,
				}
				steps = append(steps, step)
			}
		}

		newService := &api.Service{
			ProfessionalID:   payload.ProfessionalID,
			Name:             payload.Name,
			Description:      payload.Description,
			Length:           payload.Length,
			Price:            payload.Price,
			RequirePhoneCall: payload.RequirePhoneCall,
			Steps:            steps,
		}

		s, err := h.ServiceService.CreateService(newService)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if s.Steps == nil {
			s.Steps = []*api.Service{}
		}

		resp := api.ServiceResponse{
			Service: s,
		}

		Created(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleGetService() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		s, err := h.ServiceService.GetService(id)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}

			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := serviceResposne{
			Service: s,
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleUpdateService() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload serviceRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		s := &payload.Service

		if err := s.ValidateUpdate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.ServiceService.UpdateService(s); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}

			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := serviceResposne{
			Service: s,
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleDeleteService() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		if err := h.ServiceService.RemoveService(id); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}

			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{
			Message: "service deleted",
		}

		OK(w, resp, api.GetReqID(r))
	})
}
