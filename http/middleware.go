package http

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/vanoapp/api"

	"os"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/rs/zerolog"
)

func (h *Handler) authMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		jwtToken, err := h.extractToken(r)
		if err != nil {
			Error(w, err, http.StatusForbidden, api.GetReqID(r))
			return
		}

		token, err := h.parseToken(jwtToken)
		if err != nil {
			Error(w, err, http.StatusForbidden, api.GetReqID(r))
			return
		}

		blacklisted, err := h.isInBlacklist(token)
		if err != nil {
			Error(w, err, http.StatusForbidden, api.GetReqID(r))
			return
		}

		if blacklisted {
			Error(w, errors.New("invalid token"), http.StatusForbidden, api.GetReqID(r))
			return
		}

		next.ServeHTTP(w, r)
	})
}

func (h *Handler) permissionBlacklistTokenMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		jwtToken, err := h.extractToken(r)
		if err != nil {
			Error(w, err, http.StatusForbidden, api.GetReqID(r))
			return
		}

		token, err := h.parseToken(jwtToken)
		if err != nil {
			Error(w, err, http.StatusForbidden, api.GetReqID(r))
			return
		}

		blacklisted, err := h.isInBlacklist(token)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if blacklisted {
			Error(w, errors.New("invalid token"), http.StatusForbidden, api.GetReqID(r))
			return
		}

		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok {
			Error(w, errors.New("invalid claims"), http.StatusForbidden, api.GetReqID(r))
			return
		}

		scopes := claims["scopes"].(api.Scopes)

		if !inSlice(scopes.Tokens.Actions, "blacklist") {
			Error(w, errors.New("permission scope 'blacklist' not allowed"), http.StatusForbidden, api.GetReqID(r))
			return
		}

		next.ServeHTTP(w, r)
	})
}

//ContextMiddleware is
func ContextMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		k := api.ReqIDContextKey("req_id")
		ctx := context.WithValue(r.Context(), k, bson.NewObjectId().Hex())
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// LoggerMiddleware is
type LoggerMiddleware struct {
	Logger zerolog.Logger
}

//LogginMiddleware is
func (m *LoggerMiddleware) LogginMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		m.Logger.Info().Str("req_id", api.GetReqID(r)).Str("url", r.URL.String()).Msg("starting request")
		next.ServeHTTP(w, r)
		m.Logger.Info().Str("req_id", api.GetReqID(r)).Msg("finished request")
	})
}

func (h *Handler) isInBlacklist(token *jwt.Token) (bool, error) {
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return true, api.ErrInvalidJWT
	}
	issuedAt := claims["iat"].(float64)

	if issuedAt < 1508859781 {
		return true, nil
	}

	return false, nil
}

func (h *Handler) parseToken(jwtToken string) (*jwt.Token, error) {
	token, err := jwt.Parse(jwtToken, func(token *jwt.Token) (interface{}, error) {
		// Valid alg is what we expect
		if token.Method != jwt.SigningMethodHS256 {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("JWT_TOKEN")), nil
	})
	if err != nil {
		return nil, err
	}

	if !token.Valid {
		return nil, errors.New("Invalid jwt token")
	}

	return token, nil
}

func (h *Handler) extractToken(r *http.Request) (string, error) {
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		return "", errors.New("Authorization header missing")
	}

	// TODO: Make this a bit more robust, parsing-wise
	authHeaderParts := strings.Split(authHeader, " ")
	if len(authHeaderParts) != 2 || strings.ToLower(authHeaderParts[0]) != "bearer" {
		return "", errors.New("Authorization header format must be Bearer {token}")
	}

	jwtToken := authHeaderParts[1]

	return jwtToken, nil
}

func inSlice(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
