package http

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/vanoapp/api"
)

func (h *Handler) handleCreateProfessional() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload professionalRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		p := &payload.Professional

		if err := p.ValidateCreate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.ProfessionalService.CreateProfessional(p); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := professionalResponse{
			Professional: sanitizeProfessional(p),
		}

		Created(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleGetProfessional() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		p := mux.Vars(r)
		id, ok := p["id"]
		if !ok {
			Error(w, errors.New("id parameter is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		s, err := h.ProfessionalService.GetProfessional(id)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := professionalResponse{
			Professional: sanitizeProfessional(s),
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleDeleteProfessional() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		p := mux.Vars(r)
		id, ok := p["id"]
		if !ok {
			Error(w, errors.New("id parameter is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.ProfessionalService.DeleteProfessional(id); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{
			Message: "professional was deleted",
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleListProfessionals() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		p, err := h.ProfessionalService.ListProfessionals(20)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if p == nil {
			p = []*api.Professional{}
		}

		resp := professionalsResponse{
			Professionals: sanitizeProfessionals(p),
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleActivateProfessionals() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload activateProfessionalRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		switch {
		case payload.ID == "":
			Error(w, errors.New("id is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		case payload.Password == "":
			Error(w, errors.New("password is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.ProfessionalService.ActivateProfessional(payload.ID, payload.Password); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{
			Message: "professional activated",
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleListCompanyProfessionals() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		companyID, ok := v["company_id"]
		if !ok {
			Error(w, errors.New("company_id url query is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		p, err := h.ProfessionalService.ListCompanyProfessionals(companyID)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if p == nil {
			p = []*api.Professional{}
		}

		resp := professionalsResponse{
			Professionals: sanitizeProfessionals(p),
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleUpdateProfessional() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload updateProfessionalRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		us := &payload.Professional

		if err := us.ValidateUpdate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		s, err := h.ProfessionalService.UpdateProfessional(us)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := professionalResponse{
			Professional: sanitizeProfessional(s),
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleResetProfessionalPassword() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload resetPasswordRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		// take this out of here!!!!!! I gotta figure out a validation method. Either middleware or adding a method to the reuqest structs
		switch {
		case payload.ID == "":
			Error(w, errors.New("id is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		case payload.Password == "":
			Error(w, errors.New("password is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		case payload.NewPassword == "":
			Error(w, errors.New("new password is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.ProfessionalService.ResetPassword(payload.ID, payload.Password, payload.NewPassword); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{
			Message: "password was updated",
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleCheckProfessionalExists() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		q := r.URL.Query()
		phoneNumber := q.Get("phone_number")
		email := q.Get("email")

		if email == "" || phoneNumber == "" {
			Error(w, errors.New("phone number and email address are required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		s, err := h.ProfessionalService.CheckProfessionalExists(email, phoneNumber)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := professionalResponse{Professional: sanitizeProfessional(s)}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleCreateManagedProfessional() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload managedProfessionalRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		in := &api.ManagedProfessional{
			Manager:    payload.Manager,
			Managee:    payload.Managee,
			Permission: payload.Permission,
		}

		if err := in.ValidateCreate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		m, err := h.ProfessionalService.AddManagedProfessional(in)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := managedProfessionalResponse{ManagedProfessional: m}

		Created(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleUpdateManagedProfessional() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload updateManagedProfessionalRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		in := payload.ManagedProfessional
		if err := in.ValidateUpdate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		m, err := h.ProfessionalService.UpdateManagedProfessional(in)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := managedProfessionalResponse{ManagedProfessional: m}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleDeleteManagedProfessional() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		if err := h.ProfessionalService.RemoveManagedProfessional(id); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{Message: "managed professional removed"}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleListProfessionalManageProfessionals() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		spID := mux.Vars(r)["professional_id"]

		var status string
		q := r.URL.Query()
		status = q.Get("status")

		m, err := h.ProfessionalService.ListProfessionalManagedProfessionals(spID, status)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if m == nil {
			m = []*api.ManagedProfessional{}
		}

		resp := managedProfessionalsResponse{ManagedProfessionals: m}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleListProfessionalManageeInvitations() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		spID := mux.Vars(r)["professional_id"]

		var status string
		q := r.URL.Query()
		status = q.Get("status")

		m, err := h.ProfessionalService.ListProfessionalManageeInvitations(spID, status)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if m == nil {
			m = []*api.ManagedProfessional{}
		}

		resp := managedProfessionalsResponse{ManagedProfessionals: m}

		OK(w, resp, api.GetReqID(r))
	})
}

type updateProfessionalFeatureRequest struct {
	Enabled        bool   `json:"enabled"`
	ProfessionalID string `json:"professional_id"`
	FeatureID      string `json:"feature_id"`
}

func (h *Handler) handleUpdateProfessionalFeature() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]
		featureID := mux.Vars(r)["feature_id"]

		var payload updateProfessionalFeatureRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, api.ErrInvalidJSON, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.ProfessionalService.UpdateProfessionalFeature(id, featureID, payload.Enabled); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{Message: "professional feature was updated"}

		OK(w, resp, api.GetReqID(r))
	})
}

type clocInResponse struct {
	ClocIn *api.ClocIn `json:"cloc_in"`
}

type clocInsResponse struct {
	ClocIns []*api.ClocIn `json:"cloc_ins"`
}

func (h *Handler) handleClocInProfessional() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		professionalID := mux.Vars(r)["id"]

		c, err := h.ProfessionalService.ClocIn(professionalID)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := clocInResponse{ClocIn: c}

		Created(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleClocOutProfessional() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		clocInID := mux.Vars(r)["cloc_in_id"]

		if err := h.ProfessionalService.ClocOut(clocInID); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{Message: "successfully clocked out"}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleListProfessionalClocIns() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		c, err := h.ProfessionalService.ListProfessionalClocIns(id)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if c == nil {
			c = []*api.ClocIn{}
		}

		resp := clocInsResponse{ClocIns: c}

		OK(w, resp, api.GetReqID(r))
	})
}

func sanitizeProfessional(s *api.Professional) *api.SanitizedProfessional {
	services := s.Services
	if s.Services == nil {
		services = []*api.Service{}
	}
	if s.Features == nil {
		s.Features = []*api.Feature{}
	}
	return &api.SanitizedProfessional{
		ID:                 s.ID,
		CompanyID:          s.CompanyID,
		FirstName:          s.FirstName,
		LastName:           s.LastName,
		Email:              s.Email,
		PhoneNumber:        s.PhoneNumber,
		Notification:       s.Notification,
		Services:           services,
		Onboarded:          s.Onboarded,
		Permission:         s.Permission,
		PasswordSet:        s.PasswordSet,
		AdminGenerated:     s.AdminGenerated,
		RequireCC:          s.RequireCC,
		Features:           s.Features,
		Independent:        s.Independent,
		MerchantIntegrated: s.MerchantIntegrated,
	}
}

func sanitizeProfessionals(s []*api.Professional) []*api.SanitizedProfessional {
	ss := make([]*api.SanitizedProfessional, len(s))
	for x, u := range s {
		nu := sanitizeProfessional(u)
		ss[x] = nu
	}

	return ss
}
