package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/vanoapp/api"
)

type badCustomerRequest struct {
	Customer string
}

var testCompanies = []*api.Company{
	&api.Company{
		ID: "one",
	},
}

const testCustomerID = "correctid"

func TestHandleCreateCustomer(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)
	// get the server url
	url := fmt.Sprintf("%s/v1/customers", server.URL)

	tests := []struct {
		payload  interface{}
		respCode int
	}{
		{customerRequest{Customer: api.Customer{FirstName: "test", LastName: "test", PhoneNumber: "test", SelectedCompanyID: testCompanyID, Email: "test"}}, http.StatusCreated},
		{customerRequest{Customer: api.Customer{FirstName: "test", LastName: "test", PhoneNumber: "test", SelectedCompanyID: testCompanyID}}, http.StatusCreated},
		{customerRequest{Customer: api.Customer{FirstName: "test", PhoneNumber: "test", SelectedCompanyID: testCompanyID}}, http.StatusBadRequest},
		{customerRequest{Customer: api.Customer{FirstName: "test", LastName: "test", SelectedCompanyID: testCompanyID}}, http.StatusBadRequest},
		{customerRequest{Customer: api.Customer{FirstName: "test", LastName: "test", PhoneNumber: "test"}}, http.StatusCreated},
		{customerRequest{Customer: api.Customer{LastName: "test", PhoneNumber: "test", SelectedCompanyID: testCompanyID}}, http.StatusBadRequest},
		{badCustomerRequest{Customer: "muahahha"}, http.StatusBadRequest},
	}

	for i, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s, id %d", test.respCode, resp.StatusCode, string(html), i)
		}
	}
}

func TestHandleUpdateCustomer(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		payload    customerRequest
		authHeader bool
		respCode   int
	}{
		{customerRequest{Customer: api.Customer{ID: testCustomerID, FirstName: "test", LastName: "test", PhoneNumber: "test", SelectedCompanyID: testCompanyID, Email: "test", Notification: testNotification}}, true, http.StatusOK},
		{customerRequest{Customer: api.Customer{ID: testCustomerID, FirstName: "test", LastName: "test", PhoneNumber: "test", SelectedCompanyID: testCompanyID, Email: "test", Notification: testNotification}}, false, http.StatusForbidden},
		{customerRequest{Customer: api.Customer{FirstName: "test", LastName: "test", PhoneNumber: "test", SelectedCompanyID: testCompanyID, Email: "test"}}, true, http.StatusNotFound},
		{customerRequest{Customer: api.Customer{ID: testCustomerID, FirstName: "test", LastName: "test", PhoneNumber: "test", SelectedCompanyID: testCompanyID, Notification: testNotification}}, true, http.StatusOK},
		{customerRequest{Customer: api.Customer{ID: testCustomerID, FirstName: "test", PhoneNumber: "test", SelectedCompanyID: testCompanyID}}, true, http.StatusBadRequest},
		{customerRequest{Customer: api.Customer{ID: testCustomerID, FirstName: "test", LastName: "test", SelectedCompanyID: testCompanyID}}, true, http.StatusBadRequest},
		{customerRequest{Customer: api.Customer{ID: testCustomerID, FirstName: "test", LastName: "test", PhoneNumber: "test"}}, true, http.StatusBadRequest},
		{customerRequest{Customer: api.Customer{ID: testCustomerID, LastName: "test", PhoneNumber: "test", SelectedCompanyID: testCompanyID}}, true, http.StatusBadRequest},
	}

	for i, test := range tests {

		// get the server url
		url := fmt.Sprintf("%s/v1/customers/%s", server.URL, test.payload.Customer.ID)

		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("PUT", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}

		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s, id equals %d", test.respCode, resp.StatusCode, string(html), i+1)
		}
	}
}

func TestHandleGetCustomer(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		id       string
		respCode int
	}{
		{testCustomerID, http.StatusOK},
		{"incorrectid", http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/customers/%s", server.URL, test.id)
		// Build up our request
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Error(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}

func TestHandleListCompanyCustomers(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		companyID string
		respCode  int
	}{
		{testCompanyID, http.StatusOK},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/companies/%s/customers", server.URL, test.companyID)
		// Build up our request
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Error(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}

type testCheckExistsRequest struct {
	Email       string
	PhoneNumber string
}

func TestHandleCheckCustomerExists(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		req      testCheckExistsRequest
		respCode int
	}{
		{testCheckExistsRequest{Email: "correctemail", PhoneNumber: "correctnumber"}, http.StatusOK},
		{testCheckExistsRequest{Email: "correctemail", PhoneNumber: "incorrectnumber"}, http.StatusOK},
		{testCheckExistsRequest{Email: "incorrect", PhoneNumber: "correctnumber"}, http.StatusOK},
		{testCheckExistsRequest{Email: "incorrect", PhoneNumber: "incorrectnumber"}, http.StatusNotFound},
		{testCheckExistsRequest{Email: "", PhoneNumber: "incorrectnumber"}, http.StatusBadRequest},
		{testCheckExistsRequest{Email: "incorrect", PhoneNumber: ""}, http.StatusBadRequest},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/customers/check-exists?phone_number=%s&email=%s", server.URL, test.req.PhoneNumber, test.req.Email)
		// Build up our request
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Error(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}

type testCustomerCompanyRequest struct {
	CustomerID string `json:"customer_id"`
	CompanyID  string `json:"company_id"`
}

func TestHandleAddCustomerCompany(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		payload    testCustomerCompanyRequest
		authHeader bool
		statusCode int
	}{
		{testCustomerCompanyRequest{testCustomerID, testCompanyID}, true, http.StatusCreated},
		{testCustomerCompanyRequest{testCustomerID, testCompanyID}, false, http.StatusForbidden},
		{testCustomerCompanyRequest{testCustomerID, ""}, true, http.StatusBadRequest},
		{testCustomerCompanyRequest{"", testCompanyID}, true, http.StatusBadRequest},
	}

	for _, test := range tests {
		compID := test.payload.CompanyID
		if compID == "" {
			compID = "testid"
		}
		url := fmt.Sprintf("%s/v1/companies/%s/customers", server.URL, compID)

		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}

		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.statusCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.statusCode, resp.StatusCode, string(html))
		}
	}
}

func TestHandleRemoveCustomerCompany(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		payload    testCustomerCompanyRequest
		authHeader bool
		statusCode int
	}{
		{testCustomerCompanyRequest{testCustomerID, testCompanyID}, true, http.StatusOK},
		{testCustomerCompanyRequest{testCustomerID, testCompanyID}, false, http.StatusForbidden},
		{testCustomerCompanyRequest{testCustomerID, ""}, true, http.StatusBadRequest},
		{testCustomerCompanyRequest{"", testCompanyID}, true, http.StatusBadRequest},
	}

	for _, test := range tests {
		compID := test.payload.CompanyID
		if compID == "" {
			compID = "testid"
		}
		url := fmt.Sprintf("%s/v1/companies/%s/customers", server.URL, compID)

		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("DELETE", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}

		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.statusCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.statusCode, resp.StatusCode, string(html))
		}
	}
}

func TestHandleAddCustomerCard(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		payload    cardRequest
		authHeader bool
		statusCode int
	}{
		{cardRequest{api.CustomerCard{CustomerID: testCustomerID, Number: testCardNumber, Exp: "11/0222", Name: "peter griffn", Method: "visa"}}, true, http.StatusCreated},
		{cardRequest{api.CustomerCard{CustomerID: testCustomerID, Number: testCardNumber, Exp: "11/0222", Name: "peter griffn"}}, false, http.StatusForbidden},
		{cardRequest{api.CustomerCard{CustomerID: testCustomerID, Number: testCardNumber, Exp: "11/0222"}}, true, http.StatusBadRequest},
		{cardRequest{api.CustomerCard{Number: testCardNumber, Exp: "11/0222", Name: "peter griffn"}}, true, http.StatusBadRequest},
		{cardRequest{api.CustomerCard{CustomerID: testCustomerID, Exp: "11/0222", Name: "peter griffn"}}, true, http.StatusBadRequest},
		{cardRequest{api.CustomerCard{CustomerID: testCustomerID, Number: testCardNumber, Name: "peter griffn"}}, true, http.StatusBadRequest},
	}

	for i, test := range tests {
		url := fmt.Sprintf("%s/v1/cards", server.URL)

		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}

		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.statusCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("ID=%d -- Expected a %d response got: %d, resp %s", i+1, test.statusCode, resp.StatusCode, string(html))
		}
	}
}

func TestHandleRemoveCustomerCard(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		id         string
		authHeader bool
		statusCode int
	}{
		{correctid, true, http.StatusOK},
		{correctid, false, http.StatusForbidden},
		{"incorrectid", true, http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/cards/%s", server.URL, test.id)

		// Build up our request
		req, err := http.NewRequest("DELETE", url, nil)
		if err != nil {
			t.Error(err)
		}

		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}

		if resp.StatusCode != test.statusCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.statusCode, resp.StatusCode, string(html))
		}
	}
}

func TestHandleGetCustomerCards(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		customerID string
		authHeader bool
		statusCode int
	}{
		{correctid, true, http.StatusOK},
		{correctid, false, http.StatusForbidden},
		{"incorrectid", true, http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/customers/%s/cards", server.URL, test.customerID)

		// Build up our request
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Error(err)
		}

		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.statusCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.statusCode, resp.StatusCode, string(html))
		}
	}
}

func TestHandleResetCustomerPassword(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		req        resetPasswordRequest
		authHeader bool
		respCode   int
	}{
		{resetPasswordRequest{testCustomerID, "correctpassword", "newpassword"}, true, http.StatusOK},
		{resetPasswordRequest{testCustomerID, "correctpassword", "newpassword"}, false, http.StatusForbidden},
		{resetPasswordRequest{"incorrect", "cp", "np"}, true, http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/customers/%s/reset-password", server.URL, test.req.ID)

		b, err := json.Marshal(test.req)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("PUT", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}
		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}
