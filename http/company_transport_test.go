package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/vanoapp/api"
)

var testCompanyID = "correctid"

type badcompanyRequest struct {
	Company string
}

func TestCreateCompany(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)
	// get the server url
	url := fmt.Sprintf("%s/v1/companies", server.URL)

	tests := []struct {
		payload    interface{}
		authHeader bool
		respCode   int
	}{
		{companyRequest{Company: api.Company{Name: "test", AddressLine1: "test", AddressLine2: "test", City: "test", State: "as", ZipCode: 332, Country: "sdsd"}}, true, http.StatusCreated},
		{companyRequest{Company: api.Company{Name: "test", AddressLine1: "test", AddressLine2: "test", City: "test", State: "as", ZipCode: 332, Country: "sdsd"}}, false, http.StatusCreated},
		{companyRequest{Company: api.Company{Name: "test", AddressLine1: "test", City: "test", State: "as", ZipCode: 230, Country: "sdsd"}}, true, http.StatusCreated},
		{companyRequest{Company: api.Company{AddressLine1: "test", City: "test", State: "as", ZipCode: 23, Country: "sdsd"}}, true, http.StatusBadRequest},
		{companyRequest{Company: api.Company{Name: "test", City: "test", State: "as", ZipCode: 23, Country: "sdsd"}}, true, http.StatusBadRequest},
		{companyRequest{Company: api.Company{Name: "test", AddressLine1: "test", State: "as", ZipCode: 2323, Country: "sdsd"}}, true, http.StatusBadRequest},
		{companyRequest{Company: api.Company{Name: "test", AddressLine1: "test", City: "test", ZipCode: 2323, Country: "sdsd"}}, true, http.StatusBadRequest},
		{companyRequest{Company: api.Company{Name: "test", AddressLine1: "test", City: "test", State: "as", Country: "sdsd"}}, true, http.StatusBadRequest},
		{companyRequest{Company: api.Company{Name: "test", AddressLine1: "test", City: "test", State: "as", ZipCode: 23}}, true, http.StatusBadRequest},
		{companyRequest{Company: api.Company{Name: "test_company"}}, true, http.StatusBadRequest},
		{companyRequest{Company: api.Company{}}, true, http.StatusBadRequest},
		{badcompanyRequest{Company: "muahahha"}, true, http.StatusBadRequest},
	}

	for _, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}
		// Build up our request
		req, err := http.NewRequest("POST", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}

		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}

		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}

func TestGetCompany(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		id       string
		respCode int
	}{
		{testCompanyID, http.StatusOK},
		{"incorrectid", http.StatusNotFound},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/companies/%s", server.URL, test.id)
		// Build up our request
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Error(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}

func TestListCompanies(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		limit    int
		respCode int
	}{
		{5, http.StatusOK},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/companies?limit=%d", server.URL, test.limit)
		// Build up our request
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Error(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}

func TestUpdateCompany(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		payload    companyRequest
		authHeader bool
		respCode   int
	}{
		{companyRequest{Company: api.Company{ID: testCompanyID, Name: "test", AddressLine1: "test", AddressLine2: "test", City: "test", State: "as", ZipCode: 332, Country: "sdsd"}}, true, http.StatusOK},
		{companyRequest{Company: api.Company{ID: testCompanyID, Name: "test", AddressLine1: "test", AddressLine2: "test", City: "test", State: "as", ZipCode: 332, Country: "sdsd"}}, false, http.StatusForbidden},
		{companyRequest{Company: api.Company{ID: testCompanyID, Name: "test", AddressLine1: "test", City: "test", State: "as", ZipCode: 332, Country: "sdsd"}}, true, http.StatusOK},
		{companyRequest{Company: api.Company{ID: "incorrect id", Name: "test", AddressLine1: "test", City: "test", State: "as", ZipCode: 332, Country: "sdsd"}}, true, http.StatusNotFound},
		{companyRequest{Company: api.Company{Name: "test", AddressLine1: "test", AddressLine2: "test", City: "test", State: "as", ZipCode: 332, Country: "sdsd"}}, true, http.StatusNotFound},
		{companyRequest{Company: api.Company{Name: "test", AddressLine1: "test", City: "test", State: "as", ZipCode: 230, Country: "sdsd"}}, true, http.StatusNotFound},
		{companyRequest{Company: api.Company{ID: testCompanyID, AddressLine1: "test", City: "test", State: "as", ZipCode: 23, Country: "sdsd"}}, true, http.StatusBadRequest},
		{companyRequest{Company: api.Company{ID: testCompanyID, Name: "test", City: "test", State: "as", ZipCode: 23, Country: "sdsd"}}, true, http.StatusBadRequest},
		{companyRequest{Company: api.Company{ID: testCompanyID, Name: "test", AddressLine1: "test", State: "as", ZipCode: 2323, Country: "sdsd"}}, true, http.StatusBadRequest},
		{companyRequest{Company: api.Company{ID: testCompanyID, Name: "test", AddressLine1: "test", City: "test", ZipCode: 2323, Country: "sdsd"}}, true, http.StatusBadRequest},
		{companyRequest{Company: api.Company{ID: testCompanyID, Name: "test", AddressLine1: "test", City: "test", State: "as", Country: "sdsd"}}, true, http.StatusBadRequest},
		{companyRequest{Company: api.Company{ID: testCompanyID, Name: "test", AddressLine1: "test", City: "test", State: "as", ZipCode: 23}}, true, http.StatusBadRequest},
		{companyRequest{Company: api.Company{ID: testCompanyID, Name: "test_company"}}, true, http.StatusBadRequest},
		{companyRequest{Company: api.Company{ID: testCompanyID}}, true, http.StatusBadRequest},
	}

	for _, test := range tests {
		b, err := json.Marshal(test.payload)
		if err != nil {
			t.Error(err)
			return
		}

		// get the server url
		url := fmt.Sprintf("%s/v1/companies/%s", server.URL, test.payload.Company.ID)

		// Build up our request
		req, err := http.NewRequest("PUT", url, bytes.NewReader(b))
		if err != nil {
			t.Error(err)
		}
		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}

		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}
