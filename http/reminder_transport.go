package http

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	"gitlab.com/vanoapp/api"
)

func (h *Handler) handleCreateReminder() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload reminderRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		re := &payload.Reminder

		if err := re.ValidateCreate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.ReminderService.CreateReminder(re); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := reminderResponse{
			Reminder: re,
		}

		Created(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleUpdateReminder() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload updateReminderRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		re := &payload.Reminder

		if err := re.ValidateUpdate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		ur, err := h.ReminderService.UpdateReminder(re)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := reminderResponse{
			Reminder: ur,
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleListProfessionalReminders() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		p := mux.Vars(r)
		professionalID := p["professional_id"]

		q := r.URL.Query()
		limit := q.Get("limit")
		completed := q.Get("completed")

		if limit == "" || completed == "" {
			Error(w, errors.New("limit and completed query params are required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		intLimit, err := strconv.Atoi(limit)
		if err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		boolCompleted, err := strconv.ParseBool(completed)
		if err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		re, err := h.ReminderService.GetProfessionalReminders(professionalID, intLimit, boolCompleted)
		if err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		if re == nil {
			re = []*api.Reminder{}
		}

		resp := remindersResponse{
			Reminders: re,
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleGetReminder() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		reminder, err := h.ReminderService.GetReminder(id)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := reminderResponse{
			Reminder: reminder,
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleDeleteReminder() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := mux.Vars(r)["id"]

		if err := h.ReminderService.DeleteReminder(id); err != nil {
			if err == api.ErrNotFound {
				Error(w, err, http.StatusNotFound, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{
			Message: "reminder was deleted",
		}

		OK(w, resp, api.GetReqID(r))
	})
}
