package http

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetProfessionalAnalytics(t *testing.T) {
	// set up the test server
	server := httptest.NewServer(testHandler)

	tests := []struct {
		id         string
		authHeader bool
		respCode   int
	}{
		{testProfessionalID, true, http.StatusOK},
		{"incorrectid", true, http.StatusNotFound},
		{testProfessionalID, false, http.StatusForbidden},
		{"incorrectid", false, http.StatusForbidden},
	}

	for _, test := range tests {
		url := fmt.Sprintf("%s/v1/professionals/%s/analytics", server.URL, test.id)
		// Build up our request
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Error(err)
		}
		if test.authHeader {
			req.Header.Add("Authorization", testJWT)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != test.respCode {
			html, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("Expected a %d response got: %d, resp %s", test.respCode, resp.StatusCode, string(html))
		}
	}
}
