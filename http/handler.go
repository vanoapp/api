package http

import (
	"encoding/json"
	"net/http"
	"os"
	"strings"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.com/vanoapp/api"
)

// Services are all of the domain services this handler will require
type Services struct {
	CompanyService      api.CompanyService
	ProfessionalService api.ProfessionalService
	CustomerService     api.CustomerService
	AuthService         api.AuthService
	ReminderService     api.ReminderService
	AppointmentService  api.AppointmentService
	ServiceService      api.ServiceService
	ValidationService   api.ValidationService
	MerchantService     api.MerchantService
	BillingService      api.BillingService
	AuthCache           api.AuthCache
	AnalyticsService    api.AnalyticsService
	TransactionService  api.TransactionService
}

// Handler is the base handler. Any new handler type that gets created will get added here
type Handler struct {
	*mux.Router
	CompanyService      api.CompanyService
	ProfessionalService api.ProfessionalService
	CustomerService     api.CustomerService
	AuthService         api.AuthService
	ReminderService     api.ReminderService
	AppointmentService  api.AppointmentService
	ServiceService      api.ServiceService
	ValidationService   api.ValidationService
	MerchantService     api.MerchantService
	BillingService      api.BillingService
	AuthCache           api.AuthCache
	AnalyticsService    api.AnalyticsService
	TransactionService  api.TransactionService
	Logger              zerolog.Logger
}

// NewHandler is
func NewHandler(s *Services) *Handler {
	// TODO error handling to make sure all services are passed in

	h := &Handler{
		Router:              mux.NewRouter(),
		CompanyService:      s.CompanyService,
		ProfessionalService: s.ProfessionalService,
		CustomerService:     s.CustomerService,
		AuthService:         s.AuthService,
		ReminderService:     s.ReminderService,
		AppointmentService:  s.AppointmentService,
		ServiceService:      s.ServiceService,
		ValidationService:   s.ValidationService,
		MerchantService:     s.MerchantService,
		BillingService:      s.BillingService,
		AuthCache:           s.AuthCache,
		AnalyticsService:    s.AnalyticsService,
		TransactionService:  s.TransactionService,
	}

	h.getRoutes()

	return h
}

// ServeHTTP delegates a request to the appropriate subhandler.
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if strings.HasPrefix(r.URL.Path, "/v1") {
		h.Router.ServeHTTP(w, r)
	} else {
		http.NotFound(w, r)
	}
}

// Error writes an API error message to the response and logger.
func Error(w http.ResponseWriter, err error, code int, reqID string) {
	// Hide error from client if it is internal.
	if code == http.StatusInternalServerError && os.Getenv("DEBUG") != "true" {
		err = api.ErrInternal
	}

	if err == api.ErrNotFound {
		code = http.StatusNotFound
	}

	var errType string
	switch code {
	case http.StatusInternalServerError:
		errType = "Internal error"
		break
	case http.StatusBadRequest:
		errType = "Validation error"
		break
	case http.StatusNotFound:
		errType = "Not Found"
		break
	case http.StatusForbidden:
		errType = "Authentication error"
		break
	default:
		errType = "Internal error"
	}

	// Write generic error response.
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	resp := &errorResponse{Error: err.Error(), Type: errType, RequestID: reqID}
	encodeJSON(w, resp, reqID)
}

// NotFound writes an API error message to the response.
func NotFound(w http.ResponseWriter, reqID string) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotFound)
	w.Write([]byte(`{}` + "\n"))
}

// OK encodes a JSON response and sets the status to HTTP Status to 200 OK
func OK(w http.ResponseWriter, v interface{}, reqID string) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	encodeJSON(w, v, reqID)
}

// Created encodes a JSON response and sets the status to HTTP Status to 201 CREATED
func Created(w http.ResponseWriter, v interface{}, reqID string) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	encodeJSON(w, v, reqID)
}

func isValidTimestamp(t string) bool {
	switch {
	case len(t) != 10:
		return false
	case strings.Contains(t, "."):
		return false
	}

	return true
}

// encodeJSON encodes v to w in JSON format. Error() is called if encoding fails.
func encodeJSON(w http.ResponseWriter, v interface{}, reqID string) {
	l := log.Info()
	if w.Header().Get("status") == "500" {
		l = log.Error()
	}
	l.Str("req_id", reqID).Interface("body", v).Msg("response")
	if err := json.NewEncoder(w).Encode(v); err != nil {
		Error(w, err, http.StatusInternalServerError, reqID)
	}
}
