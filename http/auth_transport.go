package http

import (
	"encoding/json"
	"errors"
	"net/http"

	"gitlab.com/vanoapp/api"
)

type authenticateRequest struct {
	Role          string `json:"role"`
	Authenticator string `json:"authenticator"`
	Password      string `json:"password"`
}

type tokenRequest struct {
	Token string `json:"token"`
}

type forgotPasswordRequest struct {
	Role          string `json:"role"`
	Authenticator string `json:"authenticator"`
}

type noAuthResetPasswordRequest struct {
	ID       string `json:"id"`
	Role     string `json:"role"`
	Password string `json:"password"`
}

type tokenResponse struct {
	Token api.JWTToken `json:"token"`
}

func (h *Handler) handleAuthenticate() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload authenticateRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		c := &api.Credentials{
			Role:        payload.Role,
			Password:    payload.Password,
			PhoneNumber: payload.Authenticator,
			Email:       payload.Authenticator,
			IPAddress:   r.RemoteAddr,
		}

		if err := c.Validate(); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		t, err := h.AuthService.Authenticate(c)
		if err != nil {
			if err == api.ErrNotFound {
				Error(w, errors.New("Invalid credentials"), http.StatusBadRequest, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := tokenResponse{
			Token: t,
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleRefreshToken() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload tokenRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if payload.Token == "" {
			Error(w, errors.New("token is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		t, err := h.AuthService.RefreshToken(payload.Token)
		if err != nil {
			if err == api.ErrInvalidJWT {
				Error(w, err, http.StatusBadRequest, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := tokenResponse{
			Token: t,
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleForgotPassword() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload forgotPasswordRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if payload.Authenticator == "" {
			Error(w, errors.New("authenticator is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if payload.Role == "" {
			Error(w, errors.New("role is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if payload.Role != "professional" && payload.Role != "customer" {
			Error(w, errors.New("role must be professional or customer"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.AuthService.ForgotPassword(payload.Authenticator, payload.Role); err != nil {
			if err == api.ErrInvalidJWT {
				Error(w, err, http.StatusBadRequest, api.GetReqID(r))
				return
			}
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{
			Message: "reset password processed",
		}

		OK(w, resp, api.GetReqID(r))
	})
}

func (h *Handler) handleResetPassword() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload noAuthResetPasswordRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if payload.ID == "" {
			Error(w, errors.New("id is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if payload.Password == "" {
			Error(w, errors.New("password is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if payload.Role == "" {
			Error(w, errors.New("role is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.AuthService.ResetPassword(payload.ID, payload.Password, payload.Role); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{
			Message: "password was reset",
		}

		OK(w, resp, api.GetReqID(r))
	})
}

type blacklistTokenRequest struct {
	JTI string `json:"jti"`
}

func (h *Handler) handleBlacklistToken() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var payload blacklistTokenRequest
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			Error(w, err, http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if payload.JTI == "" {
			Error(w, errors.New("JTI is required"), http.StatusBadRequest, api.GetReqID(r))
			return
		}

		if err := h.AuthCache.Blacklist(payload.JTI); err != nil {
			Error(w, err, http.StatusInternalServerError, api.GetReqID(r))
			return
		}

		resp := messageResponse{
			Message: "token blacklisted",
		}

		OK(w, resp, api.GetReqID(r))
	})
}
