package api

import (
	"crypto/md5"
	"errors"
	"fmt"
	"io"
	"os"
	"strconv"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

//JWTBlacklist is
type JWTBlacklist struct {
	ID  int    `json:"int"`
	JTI string `json:"jti"`
}

// JWTContextKey is
type JWTContextKey string

// AuthService should be called `Authenticator`
type AuthService interface {
	Authenticate(c *Credentials) (JWTToken, error)
	RefreshToken(hashedToken string) (JWTToken, error)
	ForgotPassword(authenticator, role string) error
	ResetPassword(id, password, role string) error
}

// AuthCache is
type AuthCache interface {
	IsBlacklisted(jti string) (bool, error)
	Blacklist(jti string) error
}

// Credentials is
type Credentials struct {
	Role        string
	PhoneNumber string
	Email       string
	Password    string
	IPAddress   string
}

// Validate is
func (c *Credentials) Validate() error {
	switch {
	case c.Role == "":
		return errors.New("role is required")
	case c.Role != "professional" && c.Role != "customer":
		return errors.New("role must be either professional or customer")
	case c.Password == "":
		return errors.New("password is required")
	case c.Email == "" && c.PhoneNumber == "":
		return errors.New("email or phone number is required")
	}

	return nil
}

// JWTToken is
type JWTToken string

// Scopes is
type Scopes struct {
	Tokens *TokensScope `json:"tokens"`
}

// TokensScope is
type TokensScope struct {
	Actions []string `json:"actions"`
}

// GenerateJWT is
func GenerateJWT(id, role string) (JWTToken, error) {
	scopes := &Scopes{
		&TokensScope{
			Actions: []string{"blacklist"},
		},
	}

	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["sub"] = id
	claims["iat"] = time.Now().Unix()
	claims["exp"] = time.Now().Add(time.Hour * 720).Unix() // Expire in 30 days
	claims["nbf"] = time.Now().Unix()
	claims["iss"] = os.Getenv("BASE_URL")
	claims["scopes"] = scopes
	claims["jti"] = makeJTI(claims["sub"], claims["iat"])
	claims["role"] = role
	tokenString, err := token.SignedString([]byte(os.Getenv("JWT_TOKEN")))
	if err != nil {
		return "", fmt.Errorf("could not sign token string: %v", err)
	}
	return JWTToken(tokenString), nil
}

func makeJTI(subject, issuedAt interface{}) []byte {
	h := md5.New()
	io.WriteString(h, subject.(string))
	io.WriteString(h, strconv.Itoa(int(issuedAt.(int64))))
	return h.Sum(nil)
}

//ForgotPasswordRequestTable is
type ForgotPasswordRequestTable struct {
	TableName      struct{} `sql:"forgot_password_requests"`
	ID             string
	TextHash       string `sql:"-"`
	Hash           string
	CustomerID     string
	ProfessionalID string
	Used           bool
	CreatedAt      time.Time
	UpdatedAt      time.Time
}
