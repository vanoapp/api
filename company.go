package api

import (
	"time"

	"errors"

	uuid "github.com/satori/go.uuid"
)

// CompanyService is
type CompanyService interface {
	CreateCompany(c *Company) error
	GetCompany(id string) (*Company, error)
	ListCompanies(limit int) ([]*Company, error)
	UpdateCompany(c *Company) error
	AddCompanyImages(imgs []*CompanyImage) ([]*CompanyImage, error)
	AddCompanyImage(img *CompanyImage) (*CompanyImage, error)
	RemoveCompanyImage(id string) error
	ListCompanyImages(companyID string) ([]*CompanyImage, error)
	// GetCompanyQuery(query string, params ...interface{}) (*Company, error)
}

// Company is
type Company struct {
	ID                 string          `json:"id"`
	Name               string          `json:"name"`
	AddressLine1       string          `json:"address_line1"`
	AddressLine2       string          `json:"address_line2"`
	City               string          `json:"city"`
	State              string          `json:"state"`
	ZipCode            int             `json:"zip_code"`
	Country            string          `json:"country"`
	Hours              *Hours          `json:"hours"`
	MerchantIntegrated bool            `json:"merchant_integrated" sql:",notnull"`
	Timezone           string          `json:"timezone"`
	LogoURL            string          `json:"logo_url"`
	CoverURL           string          `json:"cover_url"`
	Images             []*CompanyImage `json:"images" sql:"-"`
	Description        string          `json:"description"`
	PhoneNumber        string          `json:"phone_number"`
	Email              string          `json:"email"`
	CreatedAt          time.Time       `json:"created_at"`
	UpdatedAt          time.Time       `json:"-"`
}

// New is
func (c *Company) New() {
	c.ID = uuid.NewV4().String()
	c.CreatedAt = time.Now()
	c.UpdatedAt = time.Now()
}

// CreateInitialHours is
func (c *Company) CreateInitialHours() {
	h := Hours{
		Monday: Time{
			Open:      "09:00",
			Close:     "17:00",
			Operating: true,
		},
		Tuesday: Time{
			Open:      "09:00",
			Close:     "17:00",
			Operating: true,
		},
		Wednesday: Time{
			Open:      "09:00",
			Close:     "17:00",
			Operating: true,
		},
		Thursday: Time{
			Open:      "09:00",
			Close:     "17:00",
			Operating: true,
		},
		Friday: Time{
			Open:      "09:00",
			Close:     "17:00",
			Operating: true,
		},
		Saturday: Time{
			Open:      "09:00",
			Close:     "17:00",
			Operating: false,
		},
		Sunday: Time{
			Open:      "09:00",
			Close:     "17:00",
			Operating: false,
		},
	}

	c.Hours = &h
}

// ValidateCreate is
func (c *Company) ValidateCreate() error {
	switch {
	case c.Name == "":
		return errors.New("company name is required")
	case c.AddressLine1 == "":
		return errors.New("address line 1 is required")
	case c.City == "":
		return errors.New("city is required")
	case c.State == "":
		return errors.New("state is required")
	case c.ZipCode == 0:
		return errors.New("zip code is required")
	case c.Country == "":
		return errors.New("country is required")
	case c.PhoneNumber == "":
		return errors.New("phone number is required")
	case c.Email == "":
		return errors.New("email address is required")
	}
	return nil
}

// ValidateUpdate is
func (c *Company) ValidateUpdate() error {
	switch {
	case c.ID == "":
		return errors.New("company id is required")
	case c.Name == "":
		return errors.New("company name is required")
	case c.AddressLine1 == "":
		return errors.New("address line 1 is required")
	case c.City == "":
		return errors.New("city is required")
	case c.State == "":
		return errors.New("state is required")
	case c.ZipCode == 0:
		return errors.New("zip code is required")
	case c.Country == "":
		return errors.New("country is required")
	case c.LogoURL == "":
		return errors.New("logo url required")
	case c.CoverURL == "":
		return errors.New("cover url required")
	case c.PhoneNumber == "":
		return errors.New("phone number is required")
	case c.Email == "":
		return errors.New("email address is required")
	}

	return nil
}

// CompanyImage is
type CompanyImage struct {
	ID        string    `json:"id"`
	CompanyID string    `json:"company_id"`
	URL       string    `json:"url"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
}

// Hours is
type Hours struct {
	Monday    Time `json:"monday"`
	Tuesday   Time `json:"tuesday"`
	Wednesday Time `json:"wednesday"`
	Thursday  Time `json:"thursday"`
	Friday    Time `json:"friday"`
	Saturday  Time `json:"saturday"`
	Sunday    Time `json:"sunday"`
}

// Time is
type Time struct {
	Open      string `json:"open"`
	Close     string `json:"close"`
	Operating bool   `json:"operating"`
}
