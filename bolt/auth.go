package bolt

import "github.com/boltdb/bolt"

// AuthCache is
type AuthCache struct {
	DB *bolt.DB
}

// Blacklist is
func (c *AuthCache) Blacklist(jti string) error {
	return c.DB.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(JWTBlacklistBucket))

		return b.Put([]byte(jti), []byte("true"))
	})
}

//IsBlacklisted is
func (c *AuthCache) IsBlacklisted(jti string) (bool, error) {
	isBlacklisted := true
	err := c.DB.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(JWTBlacklistBucket))
		v := b.Get([]byte(jti))
		if v == nil {
			isBlacklisted = false
		}
		return nil
	})
	if err != nil {
		return true, err
	}

	return isBlacklisted, nil
}
