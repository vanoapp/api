package bolt

import (
	"os"

	"github.com/boltdb/bolt"
)

const JWTBlacklistBucket = "JWTBlacklist"

// Open is
func Open(path string, mode os.FileMode, options *bolt.Options) (*bolt.DB, error) {
	db, err := bolt.Open(path, mode, options)

	// Initialize top-level buckets.
	tx, err := db.Begin(true)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	if _, err := tx.CreateBucketIfNotExists([]byte(JWTBlacklistBucket)); err != nil {
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		return nil, err
	}

	return db, nil
}
