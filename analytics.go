package api

// AnalyticsService is
type AnalyticsService interface {
	GetProfessionalAnalytics(ProfessionalID string) (*Analytics, error)
}

// Analytics is
type Analytics struct {
	Day   Stat `json:"day"`
	Month Stat `json:"month"`
	Week  Stat `json:"week"`
	Year  Stat `json:"year"`
}

// Stat is
type Stat struct {
	NewCustomers     int     `json:"new_customers"`
	PercentageBooked float32 `json:"percentage_booked"`
	NewAppointments  int     `json:"new_appointments"`
	AppointmentsLeft int     `json:"appointments_left"`
}
