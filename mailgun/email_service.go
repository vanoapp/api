package mailgun

import mailgun "github.com/mailgun/mailgun-go"

// EmailService is
type EmailService struct {
	MailClient mailgun.Mailgun
}

// SendMail is
func (s *EmailService) SendMail(to, from, subject string, body []byte) error {
	m := s.MailClient.NewMessage(
		from,
		subject,
		string(body),
		to,
	)

	m.SetHtml(string(body))
	_, _, err := s.MailClient.Send(m)
	return err
}
