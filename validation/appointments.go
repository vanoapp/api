package validation

import (
	"errors"
	"time"

	"gitlab.com/vanoapp/api"
)

// ValidateCreateAppointment is
func (s *Service) ValidateCreateAppointment(a *api.Appointment) error {
	switch {
	case a.ProfessionalID == "":
		return errors.New("stylist is required")
	case a.Services == nil:
		return errors.New("minimum 1 service is required")
	case a.CustomerID == "" && !a.IsBlockoff:
		return errors.New("customer is required")
	case a.StartTime == 0:
		return errors.New("start time is required")
	case a.EndTime == 0:
		return errors.New("end time is required")
	}

	for _, s := range a.Services {
		if s.ID == "" {
			return errors.New("service id required")
		}
	}

	if a.StartTime < time.Now().Unix() {
		return errors.New("cannot book an appointment for a date earlier than now")
	}

	return s.AppointmentService.CheckAppointmentIsAvailable(a.ProfessionalID, a.StartTime, a.EndTime)
}
