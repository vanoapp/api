package api

// ValidationService is
type ValidationService interface {
	ValidateCreateAppointment(a *Appointment) error
	ValidateCreateMerchant(m *Merchant) error
}
