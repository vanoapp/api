package api

// Notifier is
type Notifier interface {
	Notify(professionalID, customerID, msg string) error
}

// NotificationService is
type NotificationService interface {
	NotifyNewAppointment(a *Appointment) error
	NotifyCancelAppointment(a *Appointment) error
	NotifyUpdateAppointment(a *Appointment) error
	NotifyNewAdminProfessional(p *Professional) error
	NotifyManagedProfessionalInvite(managee, manager *Professional) error
	NotifyProfessionalResetPassword(p *Professional, req *ForgotPasswordRequestTable) error
	NotifyCustomerResetPassword(c *Customer, req *ForgotPasswordRequestTable) error
}

// Notification is
type Notification struct {
	SMS   bool `json:"sms"`
	Phone bool `json:"phone"`
	Email bool `json:"email"`
}
