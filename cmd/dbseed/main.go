package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"

	"gitlab.com/vanoapp/api"
	"gitlab.com/vanoapp/api/hum"
)

type customerRequest struct {
	Customer api.Customer `json:"customer"`
}

type companyRequest struct {
	Company api.Company `json:"company"`
}

func main() {
	seedCustomers()
}

func random(min, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return rand.Intn(max-min) + min
}

func seedCustomers() {
	companies := []string{"b506ea31-df5d-42fd-8e70-9bf689bd2061", "36ec09ec-6b07-45e3-ae2d-a77bfe381baa", "a00f8e4d-ebca-4350-8b96-85d02d7752e8"}

	for i := 0; i < 3000; i++ {
		c := &customerRequest{
			Customer: api.Customer{
				FirstName:         hum.FirstName(),
				LastName:          hum.LastName(),
				SelectedCompanyID: companies[random(0, len(companies))],
				Email:             hum.Email(),
				PhoneNumber:       hum.PhoneNumber(),
			},
		}

		b, err := json.Marshal(c)
		if err != nil {
			fmt.Println(err)
			continue
		}

		req, err := http.NewRequest("POST", os.Getenv("BASE_URL")+"/v1/customers", bytes.NewReader(b))
		if err != nil {
			fmt.Println(err)
			continue
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			fmt.Println(err)
			continue
		}

		log.Println(resp.StatusCode)
	}
}
