package api

import "testing"

func TestProfessional_ValidateCreate(t *testing.T) {
	tests := []struct {
		p        *Professional
		expected bool
	}{
		{&Professional{FirstName: "test", LastName: "hey", Email: "hello", Password: "hello", PhoneNumber: "2000", CompanyID: "country", Permission: &ProfessionalPermission{
			ManageAppointments:  true,
			ManageAnalytics:     true,
			ManageProfessionals: true,
			ManageCompany:       true,
		}}, true},
		{&Professional{LastName: "hey", Email: "hello", Password: "hello", PhoneNumber: "2000", CompanyID: "country"}, false},
		{&Professional{FirstName: "test", Email: "hello", Password: "hello", PhoneNumber: "2000", CompanyID: "country"}, false},
		{&Professional{FirstName: "test", LastName: "hey", Password: "hello", PhoneNumber: "2000", CompanyID: "country"}, false},
		{&Professional{FirstName: "test", LastName: "hey", Email: "hello", PhoneNumber: "2000", CompanyID: "country"}, false},
		{&Professional{FirstName: "test", LastName: "hey", Email: "hello", Password: "hello", CompanyID: "country"}, false},
		{&Professional{FirstName: "test", LastName: "hey", Email: "hello", Password: "hello", PhoneNumber: "2000"}, false},
	}

	for i, test := range tests {
		err := test.p.ValidateCreate()
		result := err == nil

		switch {
		case result != test.expected:
			t.Errorf("ID=%d -- Expecting result to be %v, got %v, with an error of %v", i+1, test.expected, result, err)
		}
	}
}

func TestUpdateProfessional_ValidateUpdate(t *testing.T) {
	tests := []struct {
		p        *UpdateProfessional
		expected bool
	}{
		{&UpdateProfessional{ID: "11", FirstName: "test", LastName: "hey", Email: "hello", PhoneNumber: "2000", Notification: &Notification{SMS: true, Email: true, Phone: true}, Permission: &ProfessionalPermission{
			ManageAppointments:  true,
			ManageAnalytics:     true,
			ManageCompany:       true,
			ManageProfessionals: true,
		}}, true},
		{&UpdateProfessional{ID: "11", FirstName: "test", LastName: "hey", Email: "hello", PhoneNumber: "2000", Notification: &Notification{SMS: true, Email: true, Phone: true}}, false},
		{&UpdateProfessional{FirstName: "test", LastName: "hey", Email: "hello", PhoneNumber: "2000"}, false},
		{&UpdateProfessional{ID: "11", LastName: "hey", Email: "hello", PhoneNumber: "2000"}, false},
		{&UpdateProfessional{ID: "11", FirstName: "test", Email: "hello", PhoneNumber: "2000"}, false},
		{&UpdateProfessional{ID: "11", FirstName: "test", LastName: "hey", PhoneNumber: "2000"}, false},
		{&UpdateProfessional{ID: "11", FirstName: "test", LastName: "hey", Email: "hello"}, false},
	}

	for i, test := range tests {
		err := test.p.ValidateUpdate()
		result := err == nil

		switch {
		case result != test.expected:
			t.Errorf("Expecting result to be %v, got %v, for test ID# %d", test.expected, result, i)
		}
	}
}
