package postgre

import (
	"testing"

	"gitlab.com/vanoapp/api"

	uuid "github.com/satori/go.uuid"
)

var testUpdatedPassword = "demo"

type testAddServiceInput struct {
	ServiceID      string
	ProfessionalID string
}

type testRemoveServiceInput struct {
	ServiceID      string
	ProfessionalID string
}

type testResetPwdInput struct {
	id       string
	password string
}

func TestCheckProfessionalExists(t *testing.T) {
	tests := []struct {
		tid      int
		payload  testCheckExists
		expected bool
	}{
		{1, testCheckExists{Email: testProfessional.Email, PhoneNumber: testProfessional.PhoneNumber}, true},
		{2, testCheckExists{Email: "nop", PhoneNumber: testProfessional.PhoneNumber}, true},
		{3, testCheckExists{Email: testProfessional.Email, PhoneNumber: "nop"}, true},
		{4, testCheckExists{Email: "nop", PhoneNumber: "nop"}, false},
	}

	for _, test := range tests {
		c, err := testProfessionalSVC.CheckProfessionalExists(test.payload.Email, test.payload.PhoneNumber)
		result := err == nil

		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v with an error of %v. ID=%d", test.expected, result, err, test.tid)
		}

		if err == nil {
			switch {
			case c == nil:
				t.Error("Expecetd customer to not be NIL if err is NIL")
			}
		}
	}
}

func TestCreateProfessional(t *testing.T) {
	tests := []struct {
		s        *api.Professional
		hasIP    bool
		expected bool
	}{
		{&api.Professional{CompanyID: testCompany.ID, FirstName: "test", LastName: "more test", Email: "demo@email.com", PhoneNumber: "9543030759", Password: "test", Permission: testPermission}, false, true},
		{&api.Professional{CompanyID: testCompany.ID, FirstName: "test", LastName: "more test", Email: "demoaas@email.com", PhoneNumber: "9542030759", Password: "test", Permission: testPermission, AdminGenerated: false, PasswordSet: true}, false, true},
		{&api.Professional{CompanyID: testCompany.ID, FirstName: "test", LastName: "more test", Email: "demo@email.com", PhoneNumber: "9543030759", Password: "test"}, false, false},
		{&api.Professional{CompanyID: testCompany.ID, FirstName: "test", LastName: "more test", Email: "wazssaa", PhoneNumber: "9543030757", Password: "test", IPAddress: "27.0.0.1", Permission: testPermission}, true, true},
		{&api.Professional{CompanyID: testCompany.ID, FirstName: "test", LastName: "more test", Email: "wazaa", PhoneNumber: "9543030759", Password: "test", IPAddress: "27.0.0.1", Permission: testPermission}, true, false},
		{&api.Professional{CompanyID: testCompany.ID, FirstName: "test", LastName: "more test", Email: "wazada", PhoneNumber: "9543030759", Permission: testPermission}, false, false},
		{&api.Professional{FirstName: "test", LastName: "more test", Email: "wadzaa", PhoneNumber: "nosadp", Password: "test", Permission: testPermission}, false, false},
		{&api.Professional{CompanyID: testCompany.ID, LastName: "more test", Email: "waazaa", PhoneNumber: "nsdop", Password: "test", Permission: testPermission}, false, false},
		{&api.Professional{CompanyID: testCompany.ID, FirstName: "test", Email: "wazdaa", PhoneNumber: "asdnop", Password: "test", Permission: testPermission}, false, false},
		{&api.Professional{CompanyID: testCompany.ID, FirstName: "test", LastName: "more test", PhoneNumber: "ndop", Password: "test", Permission: testPermission}, false, false},
		{&api.Professional{CompanyID: testCompany.ID, FirstName: "test", LastName: "more test", Email: "wasdzaa", Password: "test", Permission: testPermission}, false, false},
	}

	for i, test := range tests {
		err := testProfessionalSVC.CreateProfessional(test.s)
		result := err == nil
		if result != test.expected {
			t.Errorf("Unexpected result expecting %v, got %v. With an error: %v, id is %d", test.expected, result, err, i+1)
		}

		if err == nil {
			switch {
			case test.s.Services == nil:
				t.Error("Expecting services to not be nil")
			case test.hasIP && test.s.IPAddress == "":
				t.Error("ip_address should not be nil")
			}
		}
	}
}

func TestGetProfessional(t *testing.T) {
	tests := []struct {
		ID       string
		expected bool
	}{
		{testProfessional.ID, true},
		{uuid.NewV4().String(), false},
	}

	for _, test := range tests {
		s, err := testProfessionalSVC.GetProfessional(test.ID)

		result := err == nil
		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v, with an error of %v, id was %s", test.expected, result, err, test.ID)
		}

		if err == nil {
			switch {
			case s.Services == nil:
				t.Errorf("Expecting Professional services to not be NIL when error is NIL")
			}
		}
	}
}

func TestActivateProfessional(t *testing.T) {
	tests := []struct {
		ID       string
		password string
		expected bool
	}{
		{testProfessional.ID, testProfessionalPassword, true},
		{uuid.NewV4().String(), testProfessionalPassword, false},
	}

	for _, test := range tests {
		err := testProfessionalSVC.ActivateProfessional(test.ID, test.password)

		result := err == nil
		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v, with an error of %v, id was %s", test.expected, result, err, test.ID)
		}
	}
}

func TestListProfessional(t *testing.T) {
	tests := []struct {
		limit    int
		expected bool
	}{
		{20, true},
	}

	for _, test := range tests {
		s, err := testProfessionalSVC.ListProfessionals(test.limit)

		result := err == nil
		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v, with an error of %v", test.expected, result, err)
		}

		if err == nil {
			switch {
			case s == nil:
				t.Errorf("Expecting Professional to not be NIL when error is NIL")
			}
		}
	}
}

func TestListCompanyProfessional(t *testing.T) {
	tests := []struct {
		companyID string
		expected  bool
	}{
		{testProfessional.CompanyID, true},
		{"incorrect", false},
	}

	for _, test := range tests {
		s, err := testProfessionalSVC.ListCompanyProfessionals(test.companyID)

		result := err == nil
		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v, with an error of %v", test.expected, result, err)
		}

		if err == nil {
			switch {
			case s == nil:
				t.Errorf("Expecting Professional to not be NIL when error is NIL")
			}
		}
	}
}

func TestUpdateProfessional(t *testing.T) {
	tests := []struct {
		p        *api.UpdateProfessional
		expected bool
	}{
		{&api.UpdateProfessional{FirstName: "Joe", LastName: "s", Email: "test@email.com", PhoneNumber: "5022222222", ID: testProfessional.ID, Notification: testNotification, Permission: testPermission}, true},
		{&api.UpdateProfessional{FirstName: "Joe", LastName: "s", Email: "test@email.com", PhoneNumber: "5022222222", ID: testProfessional.ID, Notification: testNotification}, false},
		{&api.UpdateProfessional{LastName: "s", Email: "em", PhoneNumber: "sd", ID: testProfessional.ID}, false},
		{&api.UpdateProfessional{FirstName: "updated name", Email: "em", PhoneNumber: "sd", ID: testProfessional.ID}, false},
		{&api.UpdateProfessional{FirstName: "updated name", LastName: "s", PhoneNumber: "sd", ID: testProfessional.ID}, false},
		{&api.UpdateProfessional{FirstName: "updated name", LastName: "s", Email: "em", ID: testProfessional.ID}, false},
		{&api.UpdateProfessional{FirstName: "updated name", LastName: "s", Email: "em", PhoneNumber: "sd"}, false},
	}

	for i, test := range tests {
		s, err := testProfessionalSVC.UpdateProfessional(test.p)
		result := err == nil

		if result != test.expected {
			t.Errorf("Unexpected result expecting %v, got %v., with an error: %v, id is %d", test.expected, result, err, i+1)
		}

		if err == nil {
			switch {
			case s == nil:
				t.Error("Professional should not be nil if err is nil")
			}
		}
	}
}

func TestResetPassword(t *testing.T) {
	tests := []struct {
		tid      int
		p        testResetPwdInput
		expected bool
	}{
		{1, testResetPwdInput{id: testProfessional.ID, password: testProfessionalPassword}, true},
		{2, testResetPwdInput{id: "323213", password: testProfessionalPassword}, false},
		{3, testResetPwdInput{id: testProfessional.ID, password: "nop"}, false},
	}

	for _, test := range tests {
		err := testProfessionalSVC.ResetPassword(test.p.id, test.p.password, testUpdatedPassword)
		result := err == nil

		if result != test.expected {
			t.Errorf("Unexpected result expecting %v, got %v., with an error: %v, id is %d", test.expected, result, err, test.tid)
		}
	}
}

func TestOnboard(t *testing.T) {
	tests := []struct {
		tid      int
		id       string
		expected bool
	}{
		{1, testProfessional.ID, true},
		{2, "incorrect", false},
	}

	for _, test := range tests {
		err := testProfessionalSVC.Onboard(test.id)
		result := err == nil

		if result != test.expected {
			t.Errorf("Unexpected result expecting %v, got %v., with an error: %v, id is %d", test.expected, result, err, test.tid)
		}
	}
}

func TestListProfessionalManagedProfessionals(t *testing.T) {
	tests := []struct {
		id       string
		expected bool
	}{
		{testProfessional.ID, true},
		{"incorrect", true},
	}

	for i, test := range tests {
		_, err := testProfessionalSVC.ListProfessionalManagedProfessionals(test.id, "")
		result := err == nil

		if result != test.expected {
			t.Errorf("Unexpected result expecting %v, got %v., with an error: %v, id is %d", test.expected, result, err, i+1)
		}
	}
}
