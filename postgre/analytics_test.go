package postgre

import "testing"

func TestGetProfessionalAnalytics(t *testing.T) {
	tests := []struct {
		professionalID  string
		expected bool
	}{
		{testProfessional.ID, true},
		{"incorrect", false},
	}

	for i, test := range tests {
		_, err := testAnalyticsSvc.GetProfessionalAnalytics(test.professionalID)
		result := err == nil

		if result != test.expected {
			t.Errorf("Unexpected result. expecting result to be %v, got %v when id is %d, with an error: %v", test.expected, result, i+1, err)
		}
	}
}
