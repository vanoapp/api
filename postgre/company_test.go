package postgre

import (
	"testing"

	uuid "github.com/satori/go.uuid"

	"gitlab.com/vanoapp/api"
)

// This is more of a test for the database constraints. If non-null fields are erroring as expected.
func TestCreateCompany(t *testing.T) {
	tests := []struct {
		c        *api.Company
		expected bool
	}{
		{&api.Company{Name: "test company", AddressLine1: "test addr1", AddressLine2: "test addr2", City: "test city", State: "test state", ZipCode: 12345, Country: "test country", Timezone: "America/New_York", PhoneNumber: "234234", Email: "sdsasd@sdsfdf.om"}, true},
		{&api.Company{Name: "test company", AddressLine1: "test addr1", AddressLine2: "test addr2", City: "test city", State: "test state", ZipCode: 12345, Country: "test country", Timezone: "America/New_York"}, false},
		{&api.Company{Name: "test company", AddressLine1: "test addr1", AddressLine2: "test addr2", City: "test city", State: "test state", ZipCode: 12345, Country: "test country", Timezone: "America/New_York"}, false},
		{&api.Company{Name: "test company", AddressLine1: "test addr1", AddressLine2: "test addr2", City: "test city", State: "test state", ZipCode: 12345, Country: "test country"}, false},
		{&api.Company{Name: "test company", AddressLine1: "test addr1", AddressLine2: "test addr2", City: "test city", State: "test state", ZipCode: 12345, Country: "test country"}, false},
		{&api.Company{Name: "test company", AddressLine1: "test addr1", City: "test city", State: "test state", ZipCode: 12345, Country: "test country", Timezone: "America/New_York"}, false},
		{&api.Company{Name: "test company", AddressLine1: "test addr1", City: "test city", State: "test state", ZipCode: 12345, Country: "test country"}, false},
		{&api.Company{Name: "test company", City: "test city", State: "test state", ZipCode: 12345, Country: "test country"}, false},
		{&api.Company{Name: "test company", AddressLine1: "test addr1", City: "test city", State: "test state", ZipCode: 12345}, false},
		{&api.Company{Name: "test company", AddressLine1: "test addr1", City: "test city", State: "test state", Country: "test country"}, false},
		{&api.Company{Name: "test company", AddressLine1: "test addr1", City: "test city", ZipCode: 12345, Country: "test country"}, false},
		{&api.Company{Name: "test company", AddressLine1: "test addr1", State: "test state", ZipCode: 12345, Country: "test country"}, false},
		{&api.Company{Name: "test company"}, false},
		{&api.Company{Name: ""}, false},
	}

	for i, test := range tests {
		err := testCompanySvc.CreateCompany(test.c)
		result := err == nil
		if result != test.expected {
			t.Errorf("Unexpected result. expecting reuslt to be %v, got %v when id is %d, with an error: %v", test.expected, result, i+1, err)
		}

		if err == nil {
			switch {
			case test.c.ID == "":
				t.Errorf("Unexpected result. expected  id to not be nil when err is nil")
			}
		}
	}
}

// Clean this test up this is a mess
func TestGetCompany(t *testing.T) {
	tests := []struct {
		id       string
		err      error
		expected bool
	}{
		{testCompany.ID, nil, true},
		{uuid.NewV4().String(), api.ErrNotFound, false},
	}

	for _, test := range tests {
		_, err := testCompanySvc.GetCompany(test.id)
		result := err == nil

		if result != test.expected {
			t.Errorf("Unexpected result. When id is %v, with an error: %v", test.id, err)
		}

		if err != nil {
			switch {
			case err != test.err:
				t.Errorf("Unexpected result. expecting error to not be %v, got %v", test.err, err)
			}
		}

		if err == nil {
			switch {
			}
		}
	}
}

func TestListCompanies(t *testing.T) {
	c, err := testCompanySvc.ListCompanies(20)
	if err != nil {
		t.Error(err)
	}

	if err == nil && len(c) < 1 {
		t.Errorf("Unexpected result. Expecting at least one company returned")
	}
}

func TestUpdateCompany(t *testing.T) {
	tests := []struct {
		c        *api.Company
		err      error
		expected bool
	}{
		{testCompany, nil, true},
		{&api.Company{}, api.ErrNotFound, false},
		{&api.Company{ID: testCompany.ID}, nil, false},
	}

	for _, test := range tests {
		u := test.c.UpdatedAt

		err := testCompanySvc.UpdateCompany(test.c)
		result := err == nil

		if result != test.expected {
			t.Errorf("Unexpected result. When company is %v, with an error: %v", test.c, err)
		}

		if err != nil {
			switch {
			case test.err != nil && err != test.err:
				t.Errorf("Unexpected result. expecting error to not be %v, got %v", test.err, err)
			}
		}

		if err == nil {
			switch {
			case test.c.UpdatedAt == u:
				t.Errorf("Unexpected result, expecting updated_at column to be updated")
			}
		}
	}
}

func TestAddCompanyImages(t *testing.T) {
	testBadCompanyImages := []*api.CompanyImage{
		&api.CompanyImage{
			CompanyID: testCompany.ID,
			URL:       "https://res.cloudinary.com/solabuddy/image/upload/v1499454181/thumbs-up_c2bgve.jpg",
		},
		&api.CompanyImage{
			CompanyID: "nop",
			URL:       "https://res.cloudinary.com/solabuddy/image/upload/v1499454181/thumbs-up_c2bgve.jpg",
		},
	}

	testBadCompanyImages1 := []*api.CompanyImage{
		&api.CompanyImage{
			CompanyID: "nop",
			URL:       "https://res.cloudinary.com/solabuddy/image/upload/v1499454181/thumbs-up_c2bgve.jpg",
		},
		&api.CompanyImage{
			CompanyID: "nop",
			URL:       "https://res.cloudinary.com/solabuddy/image/upload/v1499454181/thumbs-up_c2bgve.jpg",
		},
	}

	tests := []struct {
		p        []*api.CompanyImage
		expected bool
	}{
		{testCompanyImages, true},
		{testBadCompanyImages, false},
		{testBadCompanyImages1, false},
	}

	for i, test := range tests {
		_, err := testCompanySvc.AddCompanyImages(test.p)
		result := err == nil
		if result != test.expected {
			t.Errorf("Unexpected result. expecting reuslt to be %v, got %v when id is %d, with an error: %v", test.expected, result, i+1, err)
		}
	}
}

func TestAddCompanyImage(t *testing.T) {
	testBadCompanyImage := &api.CompanyImage{
		CompanyID: "nop",
		URL:       "https://res.cloudinary.com/solabuddy/image/upload/v1499454181/thumbs-up_c2bgve.jpg",
	}

	tests := []struct {
		p        *api.CompanyImage
		expected bool
	}{
		{&api.CompanyImage{CompanyID: testCompany.ID, URL: "https://res.cloudinary.com/solabuddy/image/upload/v1499454181/thumbs-up_c2bgve.jpg"}, true},
		{testBadCompanyImage, false},
	}

	for i, test := range tests {
		_, err := testCompanySvc.AddCompanyImage(test.p)
		result := err == nil
		if result != test.expected {
			t.Errorf("Unexpected result. expecting reuslt to be %v, got %v when id is %d, with an error: %v", test.expected, result, i+1, err)
		}
	}
}

func TestRemoveCompanyImage(t *testing.T) {
	tests := []struct {
		id       string
		expected bool
	}{
		{testCompanyImg.ID, true},
		{"wrongid", false},
	}

	for i, test := range tests {
		err := testCompanySvc.RemoveCompanyImage(test.id)
		result := err == nil
		if result != test.expected {
			t.Errorf("Unexpected result. expecting reuslt to be %v, got %v when id is %d, with an error: %v", test.expected, result, i+1, err)
		}
	}
}
