package postgre

import (
	"testing"
	"time"

	"gitlab.com/vanoapp/api"
)

func TestCreateRemider(t *testing.T) {
	tests := []struct {
		r        *api.Reminder
		expected bool
	}{
		{&api.Reminder{Text: "test", ProfessionalID: testProfessional.ID, DueDate: time.Now().Unix()}, true},
		{&api.Reminder{Text: "test", ProfessionalID: testProfessional.ID, DueDate: time.Now().Unix(), Description: "testing description"}, true},
		{&api.Reminder{Text: "test", ProfessionalID: testProfessional.ID}, false},
		{&api.Reminder{ProfessionalID: testProfessional.ID}, false},
		{&api.Reminder{Text: "test", ProfessionalID: testProfessional.ID, DueDate: time.Now().Unix(), Completed: true}, true},
		{&api.Reminder{Text: "test"}, false},
	}

	for _, test := range tests {
		err := testReminderSVC.CreateReminder(test.r)
		result := err == nil

		if result != test.expected {
			t.Errorf("Unexpected result, expecting reuslt to be %v, got %v, with an error of %v", test.expected, result, err)
		}
	}
}

func TestUpdateRemider(t *testing.T) {
	tests := []struct {
		r        *api.UpdateReminder
		expected bool
	}{
		{&api.UpdateReminder{ID: testReminder.ID, Text: "updated test", DueDate: time.Now().Unix(), Completed: true}, true},
		{&api.UpdateReminder{ID: testReminder.ID, Text: "updated test", DueDate: time.Now().Unix(), Completed: true}, true},
		{&api.UpdateReminder{ID: "wrongid", Text: "updated test", DueDate: time.Now().Unix(), Completed: true}, false},
		{&api.UpdateReminder{ID: testReminder.ID, DueDate: time.Now().Unix(), Completed: true}, false},
	}

	for _, test := range tests {
		_, err := testReminderSVC.UpdateReminder(test.r)
		result := err == nil

		if result != test.expected {
			t.Errorf("Unexpected result, expecting reuslt to be %v, got %v, with an error of %v", test.expected, result, err)
		}
	}
}

func TestGetProfessionalRemiders(t *testing.T) {
	r, err := testReminderSVC.GetProfessionalReminders(testProfessional.ID, 5, false)
	if err != nil {
		t.Error(err)
	}

	if len(r) < 1 {
		t.Error("Expecting more than 1 reminder to be returned")
	}
}
