package postgre

import (
	"log"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"

	"github.com/satori/go.uuid"

	"golang.org/x/crypto/bcrypt"

	"gitlab.com/vanoapp/api"
	pg "gopkg.in/pg.v5"
)

// ProfessionalService is
type ProfessionalService struct {
	DB                  *pg.DB
	CompanyService      api.CompanyService
	NotificationService api.NotificationService
	SearchService       api.SearchService
}

// CreateProfessional is
// Note: refactor this to take out business logic and put it in the root domain.
func (s *ProfessionalService) CreateProfessional(p *api.Professional) error {
	p.New()
	p.Sanitize()

	if err := p.CreateInitialServices(); err != nil {
		return err
	}

	password, err := bcrypt.GenerateFromPassword([]byte(p.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	p.Password = string(password)

	// Professional values
	values := []interface{}{
		newNullString(p.ID),
		newNullString(p.CompanyID),
		newNullString(strings.ToLower(p.FirstName)),
		newNullString(strings.ToLower(p.LastName)),
		newNullString(p.PhoneNumber),
		newNullString(strings.ToLower(p.Email)),
		newNullString(p.Password),
		newNullString(p.IPAddress),
		p.Notification,
		p.AdminGenerated,
		p.PasswordSet,
		p.Permission,
		p.LastLogin,
		p.Independent,
		p.CreatedAt,
		p.UpdatedAt,
	}

	q := `
		WITH professionals as (INSERT INTO professionals(id, company_id, first_name, last_name, phone_number, email, password, ip_address, notification, admin_generated, password_set, permission, last_login, independent, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING *), services as (INSERT INTO services(id, professional_id, name, description, length, price, created_at, updated_at) VALUES
	`

	for _, row := range p.Services {
		q += "(?, (select professionals.id from professionals), ?, ?, ?, ?, ?, ?),"
		values = append(values, newNullString(row.ID), newNullString(row.Name), newNullString(row.Description), newNullInt(row.Length), newNullInt(row.Price), row.CreatedAt, row.UpdatedAt)
	}

	q = strings.TrimSuffix(q, ",")
	q += ")"

	q += `
	INSERT INTO feature_professional(id, professional_id, feature_id, enabled) VALUES 
	`

	var features []*api.Feature
	if err := s.DB.Model(&features).Select(); err != nil {
		return err
	}

	for _, f := range features {
		q += `(?, (select professionals.id from professionals), ?, ?),`
		enabled := false
		values = append(values, newNullString(uuid.NewV4().String()), newNullString(f.ID), enabled)
	}

	q = strings.TrimSuffix(q, ",")

	if _, err := s.DB.Query(p, q, values...); err != nil {
		return err
	}

	if p.AdminGenerated {
		go func() {
			if err := s.NotificationService.NotifyNewAdminProfessional(p); err != nil {
				log.Println("err sending email", err)
			}
		}()
	}

	go func(p *api.Professional) {
		profSearch := &api.SearchProfessional{
			ID:        p.ID,
			FirstName: p.FirstName,
			LastName:  p.LastName,
			Services:  p.Services,
			CompanyID: p.CompanyID,
		}

		if err := s.SearchService.AddProfessional(profSearch); err != nil {
			log.Println("err adding search prof", err)
		}
	}(p)

	return nil
}

// GetProfessional is
func (s *ProfessionalService) GetProfessional(id string) (*api.Professional, error) {
	var p api.Professional
	if err := s.DB.Model(&p).Column("*", "Services", "Features", "Services.Steps").Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	var features []*api.FeatureProfessionalTable
	if err := s.DB.Model(&features).Where("professional_id = ?", p.ID).Select(); err != nil {
		return nil, err
	}

	var services []*api.Service
	if err := s.DB.Model(&services).Where("is_master = ?", true).Select(); err != nil {
		return nil, err
	}

	p.Services = append(p.Services, services...)

	for _, svc := range p.Services {
		if len(svc.Steps) == 0 {
			svc.Steps = []*api.Service{}
		}
	}

	for _, f := range features {
		for _, pf := range p.Features {
			if pf.ID == f.ID {
				pf.Enabled = f.Enabled
			}
		}
	}
	return &p, nil
}

//AddManagedProfessional is
func (s *ProfessionalService) AddManagedProfessional(m *api.ManagedProfessional) (*api.ManagedProfessional, error) {
	newM := &api.ManagedProfessional{
		ID:         uuid.NewV4().String(),
		Permission: m.Permission,
		Managee:    m.Managee,
		Manager:    m.Manager,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}

	if err := s.DB.Insert(newM); err != nil {
		return nil, err
	}

	go func() {
		managee, err := s.GetProfessional(newM.Managee)
		if err != nil {
			log.Println("err getting sp", err)
			return
		}

		manager, err := s.GetProfessional(newM.Manager)
		if err != nil {
			log.Println("err getting sp", err)
			return
		}

		if err := s.NotificationService.NotifyManagedProfessionalInvite(managee, manager); err != nil {
			log.Println("err notifying new managed sp", err)
		}
	}()

	return newM, nil
}

// GetProfessionalQuery is
func (s *ProfessionalService) GetProfessionalQuery(query string, params ...interface{}) (*api.Professional, error) {
	var p api.Professional
	if _, err := s.DB.QueryOne(&p, query, params...); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	return &p, nil
}

// ActivateProfessional is
func (s *ProfessionalService) ActivateProfessional(id, password string) error {
	p, err := s.GetProfessional(id)
	if err != nil {
		if err == pg.ErrNoRows {
			return api.ErrNotFound
		}
		return err
	}

	cipher, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	p.Password = string(cipher)
	p.PasswordSet = true
	p.UpdatedAt = time.Now()

	if _, err := s.DB.Model(p).Column("password_set", "password", "email", "updated_at").Update(); err != nil {
		return err
	}

	return nil
}

// ListProfessionals is
func (s *ProfessionalService) ListProfessionals(limit int) ([]*api.Professional, error) {
	if limit == 0 || limit > 20 {
		limit = 20
	}
	var p []*api.Professional

	if err := s.DB.Model(&p).Column("*", "Services", "Services.Steps").Limit(limit).Select(); err != nil {
		return nil, err
	}

	return p, nil
}

// ListCompanyProfessionals is
func (s *ProfessionalService) ListCompanyProfessionals(companyID string) ([]*api.Professional, error) {
	var p []*api.Professional
	if err := s.DB.Model(&p).Column("*", "Services", "Features", "Services.Steps").Where("company_id = ?", companyID).Select(); err != nil {
		return nil, err
	}

	if len(p) == 0 {
		return nil, api.ErrNotFound
	}

	return p, nil
}

// UpdateProfessional is
func (s *ProfessionalService) UpdateProfessional(u *api.UpdateProfessional) (*api.Professional, error) {
	p, err := s.GetProfessional(u.ID)
	if err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}

		return nil, err
	}

	p.FirstName = u.FirstName
	p.LastName = u.LastName
	p.Email = u.Email
	p.PhoneNumber = u.PhoneNumber
	p.Notification = u.Notification
	p.LastLogin = u.LastLogin
	p.IPAddress = u.IPAddress
	p.UpdatedAt = time.Now()
	p.Onboarded = true
	p.Permission = u.Permission
	p.RequireCC = u.RequireCC
	p.MerchantIntegrated = u.MerchantIntegrated

	if _, err := s.DB.Model(p).Column("first_name", "last_name", "email", "phone_number", "notification", "last_login", "ip_address", "updated_at", "onboarded", "permission", "require_cc", "merchant_integrated").Update(); err != nil {
		return nil, err
	}

	return p, nil
}

// DeleteProfessional is
func (s *ProfessionalService) DeleteProfessional(id string) error {
	p, err := s.GetProfessional(id)
	if err != nil {
		if err == pg.ErrNoRows {
			return api.ErrNotFound
		}

		return err
	}

	if _, err := s.DB.Model(p).Delete(); err != nil {
		return err
	}

	return nil
}

// ResetPassword will call the database, get the Professional, check if the password hash matches the database hash, if succeeds then it will store the new password in the database
func (s *ProfessionalService) ResetPassword(id, password, newPassword string) error {
	p, err := s.GetProfessional(id)
	if err != nil {
		if err == pg.ErrNoRows {
			return api.ErrNotFound
		}
		return err
	}

	if err := bcrypt.CompareHashAndPassword([]byte(p.Password), []byte(password)); err != nil {
		return api.ErrNotFound
	}

	pass, err := bcrypt.GenerateFromPassword([]byte(newPassword), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	p.Password = string(pass)
	p.UpdatedAt = time.Now()

	if _, err := s.DB.Model(p).Column("password", "updated_at").Update(); err != nil {
		return err
	}

	return nil
}

// Onboard is
func (s *ProfessionalService) Onboard(id string) error {
	p, err := s.GetProfessional(id)
	if err != nil {
		return err
	}
	p.UpdatedAt = time.Now()
	p.Onboarded = true

	if _, err := s.DB.Model(p).Column("onboarded", "updated_at").Update(); err != nil {
		return err
	}

	return nil
}

// UpdateAuth is
func (s *ProfessionalService) UpdateAuth(id, ipAddress string, lastLogin time.Time) error {
	p, err := s.GetProfessional(id)
	if err != nil {
		if err == pg.ErrNoRows {
			return api.ErrNotFound
		}

		return err
	}

	p.LastLogin = lastLogin
	p.IPAddress = ipAddress
	p.UpdatedAt = time.Now()

	if _, err := s.DB.Model(p).Column("last_login", "ip_address", "updated_at").Update(); err != nil {
		return err
	}

	return nil
}

// CheckProfessionalExists is
func (s *ProfessionalService) CheckProfessionalExists(email, phoneNumber string) (*api.Professional, error) {
	var p api.Professional
	if err := s.DB.Model(&p).Where("email = ?", email).WhereOr("phone_number = ?", phoneNumber).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	return &p, nil
}

//ListProfessionalManagedProfessionals is
func (s *ProfessionalService) ListProfessionalManagedProfessionals(spID, status string) ([]*api.ManagedProfessional, error) {
	var sps []*api.ManagedProfessional
	if len(status) > 0 {
		if err := s.DB.Model(&sps).Where("manager = ? and status = ?", spID, status).Select(); err != nil {
			return []*api.ManagedProfessional{}, err
		}
	} else {
		if err := s.DB.Model(&sps).Where("manager = ?", spID).Select(); err != nil {
			return []*api.ManagedProfessional{}, err
		}
	}

	return sps, nil
}

// ListProfessionalManageeInvitations is
func (s *ProfessionalService) ListProfessionalManageeInvitations(spID, status string) ([]*api.ManagedProfessional, error) {
	var sps []*api.ManagedProfessional
	if len(status) > 0 {
		if err := s.DB.Model(&sps).Where("managee = ? and status = ?", spID, status).Select(); err != nil {
			return []*api.ManagedProfessional{}, err
		}
	} else {
		if err := s.DB.Model(&sps).Where("managee = ?", spID).Select(); err != nil {
			return []*api.ManagedProfessional{}, err
		}
	}
	return sps, nil
}

// UpdateManagedProfessional is
func (s *ProfessionalService) UpdateManagedProfessional(m *api.ManagedProfessional) (*api.ManagedProfessional, error) {
	p, err := s.GetManagedProfessional(m.ID)
	if err != nil {
		return nil, err
	}

	p.Permission = m.Permission
	p.Status = m.Status
	p.UpdatedAt = time.Now()

	if _, err := s.DB.Model(p).Column("permission", "status", "updated_at").Update(); err != nil {
		return nil, err
	}

	return p, nil
}

// GetManagedProfessional is
func (s *ProfessionalService) GetManagedProfessional(id string) (*api.ManagedProfessional, error) {
	var p api.ManagedProfessional
	if err := s.DB.Model(&p).Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	return &p, nil
}

// RemoveManagedProfessional is
func (s *ProfessionalService) RemoveManagedProfessional(id string) error {
	p, err := s.GetManagedProfessional(id)
	if err != nil {
		if err == pg.ErrNoRows {
			return api.ErrNotFound
		}

		return err
	}

	if _, err := s.DB.Model(p).Delete(); err != nil {
		return err
	}

	return nil
}

//AddForgotPasswordRequest is
func (s *ProfessionalService) AddForgotPasswordRequest(id string) (*api.ForgotPasswordRequestTable, error) {
	req := new(api.ForgotPasswordRequestTable)
	req.ID = uuid.NewV4().String()
	req.ProfessionalID = id
	req.CreatedAt = time.Now()
	req.UpdatedAt = time.Now()
	req.TextHash = bson.NewObjectId().Hex()
	req.Hash = api.GenerateMD5Hash(req.TextHash)

	if err := s.DB.Insert(req); err != nil {
		return nil, err
	}

	return req, nil
}

//NoAuthResetRequest is
func (s *ProfessionalService) NoAuthResetRequest(plainHash, newPassword string) error {
	r := new(api.ForgotPasswordRequestTable)
	hashedText := api.GenerateMD5Hash(plainHash)
	if err := s.DB.Model(r).Where("hash = ?", hashedText).Where("used = ?", false).First(); err != nil {
		if err == pg.ErrNoRows {
			return api.ErrNotFound
		}
		return err
	}

	p, err := s.GetProfessional(r.ProfessionalID)
	if err != nil {
		return err
	}

	pass, err := bcrypt.GenerateFromPassword([]byte(newPassword), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	p.Password = string(pass)
	p.UpdatedAt = time.Now()

	if _, err := s.DB.Model(p).Column("password", "updated_at").Update(); err != nil {
		return err
	}

	r.Used = true
	r.UpdatedAt = time.Now()

	if _, err := s.DB.Model(r).Column("used", "updated_at").Update(); err != nil {
		return err
	}

	return nil
}

// UpdateProfessionalFeature is
func (s *ProfessionalService) UpdateProfessionalFeature(id, featureID string, enabled bool) error {
	f := new(api.Feature)
	if err := s.DB.Model(f).Where("id = ?", featureID).First(); err != nil {
		if err == pg.ErrNoRows {
			return api.ErrNotFound
		}
		return err
	}

	if !f.CompanyWide {
		spFeature := new(api.FeatureProfessionalTable)
		if err := s.DB.Model(spFeature).Where("professional_id = ? and feature_id = ?", id, featureID).First(); err != nil {
			if err == pg.ErrNoRows {
				return api.ErrNotFound
			}
			return err
		}

		spFeature.Enabled = enabled

		return s.DB.Update(spFeature)
	}

	p, err := s.GetProfessional(id)
	if err != nil {
		return err
	}

	sps, err := s.ListCompanyProfessionals(p.CompanyID)
	if err != nil {
		return err
	}

	for _, sp := range sps {
		spFeature := new(api.FeatureProfessionalTable)
		if err := s.DB.Model(spFeature).Where("professional_id = ? and feature_id = ?", sp.ID, featureID).First(); err != nil {
			if err == pg.ErrNoRows {
				return api.ErrNotFound
			}
			return err
		}

		spFeature.Enabled = enabled

		if err := s.DB.Update(spFeature); err != nil {
			return err
		}
	}

	return nil
}

// ClocIn is
func (s *ProfessionalService) ClocIn(professionalID string) (*api.ClocIn, error) {
	if _, err := s.GetProfessional(professionalID); err != nil {
		return nil, err
	}

	newClocIn := &api.ClocIn{
		ID:             uuid.NewV4().String(),
		ProfessionalID: professionalID,
		ClocInTime:     time.Now(),
		CreatedAt:      time.Now(),
		UpdatedAt:      time.Now(),
	}

	if err := s.DB.Insert(newClocIn); err != nil {
		return nil, err
	}

	return newClocIn, nil
}

// ClocOut is
func (s *ProfessionalService) ClocOut(clocinID string) error {
	clocIn := new(api.ClocIn)
	if err := s.DB.Model(clocIn).Where("id = ?", clocinID).First(); err != nil {
		if err == pg.ErrNoRows {
			return api.ErrNotFound
		}
		return err
	}

	clocIn.ClocOutTime = time.Now()
	clocIn.UpdatedAt = time.Now()

	return s.DB.Update(clocIn)
}

//ListProfessionalClocIns is
func (s *ProfessionalService) ListProfessionalClocIns(id string) ([]*api.ClocIn, error) {
	var c []*api.ClocIn
	if err := s.DB.Model(&c).Where("professional_id = ?", id).Order("created_at").Select(); err != nil {
		return nil, err
	}

	return c, nil
}
