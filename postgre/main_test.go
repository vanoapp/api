package postgre

import (
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"github.com/joho/godotenv"
	uuid "github.com/satori/go.uuid"

	"gitlab.com/vanoapp/api"
	"gitlab.com/vanoapp/api/hum"
	"gitlab.com/vanoapp/api/mock"
	"golang.org/x/crypto/bcrypt"
	pg "gopkg.in/pg.v5"
)

var (
	testCompanySvc   *CompanyService
	testCompanyName  = "test company"
	testCompany      *api.Company
	testProfessional *api.Professional
	// needed to help test the GetProfessional method to check if services is not nil
	testCreatedProfessional  *api.Professional
	testService              *api.Service
	testServiceSvc           api.ServiceService
	testTransactionSvc       api.TransactionService
	testDb                   *pg.DB
	testProfessionalSVC      *ProfessionalService
	testMerchantSVC          *MerchantService
	testReminderSVC          *ReminderService
	testAnalyticsSvc         *AnalyticsService
	testReminder             *api.Reminder
	testAppointmentSVC       *AppointmentService
	testAppointment          *api.Appointment
	testCustomerSVC          *CustomerService
	testCustomer             *api.Customer
	testCustomerNote         *api.CustomerNote
	testProfessionalPassword = "testpass"
	testCustomerPassword     = "testpass"
	testNotification         = &api.Notification{
		SMS:   true,
		Email: true,
		Phone: false,
	}
	testPermission = &api.ProfessionalPermission{
		ManageAppointments:  true,
		ManageCompany:       true,
		ManageAnalytics:     true,
		ManageProfessionals: true,
	}
	testCustomerCard  *api.CustomerCard
	testCompanyImages []*api.CompanyImage
	testCompanyImg    *api.CompanyImage
)

func TestMain(m *testing.M) {
	if err := godotenv.Load("../.env"); err != nil {
		log.Fatal("Error loading .env file")
	}

	testDb = pg.Connect(&pg.Options{
		User:     os.Getenv("DB_USER"),
		Addr:     os.Getenv("DB_ADDR"),
		Database: os.Getenv("DB_NAME"),
		Password: os.Getenv("DB_PASSWORD"),
	})

	defer testDb.Close()

	cleanup()
	setup()

	t := m.Run()

	// cleanup()

	os.Exit(t)
}

func setup() {
	testSearchService := &mock.SearchService{}

	testCompanySvc = &CompanyService{
		DB:            testDb,
		SearchService: testSearchService,
	}

	testProfessionalSVC = &ProfessionalService{
		DB:             testDb,
		CompanyService: testCompanySvc,
		SearchService:  testSearchService,
	}

	testAnalyticsSvc = &AnalyticsService{
		ProfessionalService: testProfessionalSVC,
		DB:                  testDb,
	}

	testServiceSvc = &ServiceService{
		DB: testDb,
	}

	testAppointmentSVC = &AppointmentService{
		DB:                  testDb,
		NotificationService: &mock.NotificationService{},
		ProfessionalService: &mock.ProfessionalService{},
		CustomerService:     &mock.CustomerService{},
	}

	testCustomerSVC = &CustomerService{
		DB:            testDb,
		SearchService: testSearchService,
	}

	testReminderSVC = &ReminderService{
		DB: testDb,
	}

	testMerchantSVC = &MerchantService{
		DB: testDb,
	}

	testTransactionSvc = &TransactionService{
		DB: testDb,
	}

	c, err := createCompany()
	if err != nil {
		log.Fatal(err)
	}
	testCompany = c
	testProfessional = createProfessional(c.ID)
	createService()
	createCustomer(c)
	createReminder()
	createAppointment()
	testCompanyImages = []*api.CompanyImage{
		&api.CompanyImage{
			CompanyID: testCompany.ID,
			URL:       "https://res.cloudinary.com/solabuddy/image/upload/v1499454181/thumbs-up_c2bgve.jpg",
		},
		&api.CompanyImage{
			CompanyID: testCompany.ID,
			URL:       "https://res.cloudinary.com/solabuddy/image/upload/v1499454181/thumbs-up_c2bgve.jpg",
		},
	}
	testCompanyImg = &api.CompanyImage{
		ID:        uuid.NewV4().String(),
		CompanyID: testCompany.ID,
		URL:       "https://res.cloudinary.com/solabuddy/image/upload/v1499454181/thumbs-up_c2bgve.jpg",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	if err := testDb.Insert(testCompanyImg); err != nil {
		log.Fatal(err)
	}
}

func createCompany() (*api.Company, error) {
	c := &api.Company{
		ID:           uuid.NewV4().String(),
		Name:         hum.Company(),
		AddressLine1: hum.Address(),
		AddressLine2: "test addr2",
		City:         hum.City(),
		State:        hum.StateAbbr(),
		ZipCode:      hum.ZipCode(),
		Country:      "test country",
		Timezone:     "America/New_York",
		PhoneNumber:  "34w3324234",
		Email:        "ro@ro.comzz",
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
	}

	c.CreateInitialHours()

	if err := testDb.Insert(c); err != nil {
		return nil, err
	}

	return c, nil
}

func createProfessional(companyID string) *api.Professional {
	fmt.Println("creating Professional")
	p, err := bcrypt.GenerateFromPassword([]byte(testProfessionalPassword), 10)
	if err != nil {
		log.Fatal("error generating Professional password", err)
	}

	s := api.Professional{
		ID:           uuid.NewV4().String(),
		CompanyID:    companyID,
		FirstName:    hum.FirstName(),
		LastName:     hum.LastName(),
		Email:        "test@email.com",
		PhoneNumber:  hum.PhoneNumber(),
		Password:     string(p),
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		Notification: testNotification,
		Permission:   testPermission,
	}

	s.CreateInitialServices()

	if err := testDb.Insert(&s); err != nil {
		log.Fatal("err inserting Professional", err)
	}

	return &s
}

func createRandProfessional(companyID string) *api.Professional {
	p, err := bcrypt.GenerateFromPassword([]byte(testProfessionalPassword), 10)
	if err != nil {
		log.Fatal("error generating Professional password", err)
	}

	s := api.Professional{
		ID:           uuid.NewV4().String(),
		CompanyID:    companyID,
		FirstName:    hum.FirstName(),
		LastName:     hum.LastName(),
		Email:        hum.Email(),
		PhoneNumber:  hum.PhoneNumber(),
		Password:     string(p),
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		Notification: testNotification,
		Permission:   testPermission,
	}

	s.CreateInitialServices()

	if err := testDb.Insert(&s); err != nil {
		log.Fatal("err inserting Professional", err)
	}

	return &s
}

func createCustomer(company *api.Company) {
	p, err := bcrypt.GenerateFromPassword([]byte(testCustomerPassword), 10)
	if err != nil {
		log.Fatal("error generating Professional password", err)
	}
	c := api.Customer{
		ID:                uuid.NewV4().String(),
		SelectedCompanyID: company.ID,
		FirstName:         "test first name",
		LastName:          "test last name",
		Email:             "sup@emsail.com",
		PhoneNumber:       "9234923as943",
		Password:          string(p),
		CreatedAt:         time.Now(),
		UpdatedAt:         time.Now(),
		Notification:      testNotification,
	}

	if err := testDb.Insert(&c); err != nil {
		log.Fatal(err)
	}

	if err := testDb.Insert(&api.CompanyCustomer{
		ID:         uuid.NewV4().String(),
		CompanyID:  company.ID,
		CustomerID: c.ID,
	}); err != nil {
		log.Fatal(err)
	}

	testCustomer = &c

	testCustomerCard = &api.CustomerCard{
		ID:         uuid.NewV4().String(),
		CustomerID: testCustomer.ID,
		LastFour:   3598,
		Exp:        "11/27",
		Name:       "Peter Gregory",
		Token:      "testtoken",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}

	if err := testDb.Insert(testCustomerCard); err != nil {
		log.Fatal("error insertint customer card", err)
	}

	testCustomerNote = &api.CustomerNote{
		CustomerID: testCustomer.ID,
		Value:      "test main note",
	}

	testCustomerNote.New()

	if err := testDb.Insert(testCustomerNote); err != nil {
		log.Fatal(err)
	}
}

func createService() {
	if err := testDb.Insert(&testProfessional.Services); err != nil {
		log.Fatal("err creating services", err)
	}

	testService = testProfessional.Services[0]
}

func createAppointment() error {
	id := uuid.NewV4().String()

	a := &api.Appointment{
		ID:             id,
		ProfessionalID: testProfessional.ID,
		CustomerID:     testCustomer.ID,
		Notes:          "hello from main",
		StartTime:      time.Now().Unix(),
		EndTime:        time.Now().Add(30 * time.Minute).Unix(),
		CreatedAt:      time.Now(),
		UpdatedAt:      time.Now(),
	}

	if err := testDb.Insert(a); err != nil {
		log.Fatal(err)
	}

	as := &api.ServiceAppointment{
		ID:            uuid.NewV4().String(),
		AppointmentID: id,
		ServiceID:     testService.ID,
	}

	if err := testDb.Insert(as); err != nil {
		log.Fatal(err)
	}

	testAppointment = a

	return nil
}

func createReminder() {
	r := &api.Reminder{
		ID:             uuid.NewV4().String(),
		ProfessionalID: testProfessional.ID,
		Text:           "test",
		DueDate:        time.Now().Unix(),
		CreatedAt:      time.Now(),
		UpdatedAt:      time.Now(),
	}

	if err := testDb.Insert(r); err != nil {
		log.Fatal(err)
	}

	testReminder = r
}

func cleanup() {
	if _, err := testDb.Query(api.Company{}, "TRUNCATE companies, professionals, customers, merchants, appointments CASCADE"); err != nil {
		fmt.Println("error on db cleanup", err)
	}
}
