package postgre

import (
	"testing"

	"gitlab.com/vanoapp/api"
)

func TestCreateCustomer(t *testing.T) {
	tests := []struct {
		c        *api.Customer
		expected bool
	}{
		{&api.Customer{FirstName: "t", LastName: "g", SelectedCompanyID: testCompany.ID, Email: "asdd", Password: "p", PhoneNumber: "asdp"}, true},
		{&api.Customer{FirstName: "t", LastName: "g", SelectedCompanyID: testCompany.ID, Email: "asdd", Password: "p", PhoneNumber: "asdp"}, false},
		{&api.Customer{FirstName: "t", LastName: "g", SelectedCompanyID: testCompany.ID, Email: "dsad", PhoneNumber: "asddp"}, true},
		{&api.Customer{FirstName: "t", LastName: "g", SelectedCompanyID: testCompany.ID, Password: "p", PhoneNumber: "ddp"}, true},
		{&api.Customer{FirstName: "t", LastName: "g", SelectedCompanyID: testCompany.ID, PhoneNumber: "sadp"}, true},
		{&api.Customer{LastName: "g", SelectedCompanyID: testCompany.ID, Password: "p", PhoneNumber: "asddsp"}, false},
		{&api.Customer{FirstName: "t", SelectedCompanyID: testCompany.ID, Password: "p", PhoneNumber: "asdsdp"}, false},
		{&api.Customer{FirstName: "t", LastName: "g", Password: "p", PhoneNumber: "pasd"}, true},
		{&api.Customer{FirstName: "t", LastName: "g", SelectedCompanyID: testCompany.ID, Password: "pasd"}, false},
	}

	for i, test := range tests {
		plainPassword := test.c.Password
		err := testCustomerSVC.CreateCustomer(test.c)
		result := err == nil
		if result != test.expected {
			t.Errorf("Unexpected result. expecting reuslt to be %v, got %v when id is %d, with an error: %v", test.expected, result, i+1, err)
		}

		if err == nil {
			switch {
			case plainPassword != "" && plainPassword == test.c.Password:
				t.Errorf("password should be hashed. id %d", i+1)
			}
		}
	}
}
func TestGetCustomer(t *testing.T) {
	tests := []struct {
		tid      int
		id       string
		expected bool
	}{
		{1, testCustomer.ID, true},
		{2, "none exsiting", false},
	}

	for _, test := range tests {
		c, err := testCustomerSVC.GetCustomer(test.id)
		result := err == nil

		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v with an error of %v. ID=%d", test.expected, result, err, test.tid)
		}

		if err == nil {
			switch {
			case c == nil:
				t.Error("Expecetd customer to not be NIL if err is NIL")
			}
		}
	}
}
func TestGetCustomerCards(t *testing.T) {
	tests := []struct {
		id       string
		expected bool
	}{
		{testCustomer.ID, true},
		{"none exsiting", false},
	}

	for i, test := range tests {
		c, err := testCustomerSVC.GetCustomerCards(test.id)
		result := err == nil

		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v with an error of %v. ID=%d", test.expected, result, err, i+1)
		}

		if err == nil {
			switch {
			case c == nil:
				t.Error("Expecetd customer cards to not be NIL if err is NIL")
			}
		}
	}
}

func TestListCompanyCustomers(t *testing.T) {
	tests := []struct {
		companyID string
		page      int
		results   bool
		expected  bool
	}{
		{testCompany.ID, 1, true, true},
		{testCompany.ID, 2, false, true},
		{"incorrectID", 1, false, true},
	}

	for i, test := range tests {
		c, err := testCustomerSVC.ListCompanyCustomers(test.companyID, test.page)
		result := err == nil

		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v with an error of %v. ID=%d", test.expected, result, err, i+1)
		}

		if err == nil && test.results {
			switch {
			case len(c) <= 0:
				t.Error("Expected customer response length to be greater than 0 if err is NIL")
			}
		}
	}
}
func TestUpdateCustomer(t *testing.T) {
	tests := []struct {
		c        *api.Customer
		expected bool
	}{
		{testCustomer, true},
		{&api.Customer{ID: "nop"}, false},
	}

	for i, test := range tests {
		updatedAt := test.c.UpdatedAt
		err := testCustomerSVC.UpdateCustomer(test.c)
		result := err == nil
		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v with an error of %v. ID=%d", test.expected, result, err, i)
		}

		if err == nil {
			switch {
			case test.c.UpdatedAt == updatedAt:
				t.Error("Expeceting update at time to update")
			}
		}
	}
}

func TestAddCustomerNote(t *testing.T) {
	tests := []struct {
		tid      int
		cn       *api.CustomerNote
		expected bool
	}{
		{1, &api.CustomerNote{CustomerID: testCustomer.ID, Value: "sadsad"}, true},
		{2, &api.CustomerNote{CustomerID: "incorrect", Value: "asdsd"}, false},
		{3, &api.CustomerNote{CustomerID: "incorrect"}, false},
	}

	for _, test := range tests {
		err := testCustomerSVC.AddCustomerNote(test.cn)
		result := err == nil
		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v with an error of %v. ID=%d", test.expected, result, err, test.tid)
		}

		if err == nil {
			switch {
			case test.cn.ID == "":
				t.Error("Expecetd ID to be set")
			}
		}
	}
}

func TestUpdateCustomerNote(t *testing.T) {
	tests := []struct {
		tid      int
		cn       *api.CustomerNote
		expected bool
	}{
		{1, testCustomerNote, true},
		{2, &api.CustomerNote{ID: "incorrect", CustomerID: "incorrect", Value: "asdsd"}, false},
	}

	for _, test := range tests {
		err := testCustomerSVC.UpdateCustomerNote(test.cn)
		result := err == nil
		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v with an error of %v. ID=%d", test.expected, result, err, test.tid)
		}
	}
}

func TestRemoveCustomerNote(t *testing.T) {
	tests := []struct {
		tid      int
		id       string
		expected bool
	}{
		{1, testCustomerNote.ID, true},
		{2, "incorrect", false},
	}

	for _, test := range tests {
		err := testCustomerSVC.RemoveCustomerNote(test.id)
		result := err == nil
		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v with an error of %v. ID=%d", test.expected, result, err, test.tid)
		}
	}
}

type testCheckExists struct {
	Email       string
	PhoneNumber string
}

func TestCheckCustomerExists(t *testing.T) {
	tests := []struct {
		tid      int
		payload  testCheckExists
		expected bool
	}{
		{1, testCheckExists{Email: testCustomer.Email, PhoneNumber: testCustomer.PhoneNumber}, true},
		{2, testCheckExists{Email: "nop", PhoneNumber: testCustomer.PhoneNumber}, true},
		{3, testCheckExists{Email: testCustomer.Email, PhoneNumber: "nop"}, true},
		{4, testCheckExists{Email: "nop", PhoneNumber: "nop"}, false},
	}

	for _, test := range tests {
		c, err := testCustomerSVC.CheckCustomerExists(test.payload.Email, test.payload.PhoneNumber)
		result := err == nil

		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v with an error of %v. ID=%d", test.expected, result, err, test.tid)
		}

		if err == nil {
			switch {
			case c == nil:
				t.Error("Expecetd customer to not be NIL if err is NIL")
			}
		}
	}
}

func TestAddCustomerCompany(t *testing.T) {
	tests := []struct {
		cn       *api.CompanyCustomer
		expected bool
	}{
		{&api.CompanyCustomer{CustomerID: testCustomer.ID, CompanyID: testCompany.ID}, true},
		{&api.CompanyCustomer{CompanyID: testCompany.ID}, false},
		{&api.CompanyCustomer{CustomerID: testCustomer.ID}, false},
		{&api.CompanyCustomer{CustomerID: "nop", CompanyID: testCompany.ID}, false},
		{&api.CompanyCustomer{CustomerID: testCustomer.ID, CompanyID: "nop"}, false},
	}

	for i, test := range tests {
		err := testCustomerSVC.AddCustomerCompany(test.cn.CustomerID, test.cn.CompanyID)
		result := err == nil
		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v with an error of %v. ID=%d", test.expected, result, err, i+1)
		}
	}
}

func TestRemoveCustomerCompany(t *testing.T) {
	tests := []struct {
		cn       *api.CompanyCustomer
		expected bool
	}{
		{&api.CompanyCustomer{CustomerID: testCustomer.ID, CompanyID: testCompany.ID}, true},
		{&api.CompanyCustomer{CompanyID: testCompany.ID}, false},
		{&api.CompanyCustomer{CustomerID: testCustomer.ID}, false},
		{&api.CompanyCustomer{CustomerID: "nop", CompanyID: testCompany.ID}, false},
		{&api.CompanyCustomer{CustomerID: testCustomer.ID, CompanyID: "nop"}, false},
	}

	for i, test := range tests {
		err := testCustomerSVC.RemoveCustomerCompany(test.cn.CustomerID, test.cn.CompanyID)
		result := err == nil
		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v with an error of %v. ID=%d", test.expected, result, err, i+1)
		}
	}
}

func TestAddCustomerCard(t *testing.T) {
	tests := []struct {
		c        *api.CustomerCard
		expected bool
	}{
		{&api.CustomerCard{CustomerID: testCustomer.ID, Number: "4444444444444444", Exp: "11/27", Name: "peter griffin", Token: "sdsfsdf"}, true},
		{&api.CustomerCard{CustomerID: testCustomer.ID, Number: "4444444444444444", Exp: "11/27", Name: "peter griffin"}, true},
		{&api.CustomerCard{Number: "4444444444444444", Exp: "11/27", Name: "peter griffin", Token: "3123213"}, false},
		{&api.CustomerCard{CustomerID: "nop", Number: "4444444444444444", Exp: "11/27", Name: "peter griffin", Token: "3123213"}, false},
		{&api.CustomerCard{CustomerID: testCustomer.ID, Number: "4444444444444444", Exp: "11/27", Token: "3123213"}, false},
		{&api.CustomerCard{CustomerID: testCustomer.ID, Number: "4444444444444444", Name: "peter griffin", Token: "3123213"}, false},
		{&api.CustomerCard{CustomerID: testCustomer.ID, Exp: "11/27", Name: "peter griffin", Token: "3123213"}, false},
	}

	for i, test := range tests {
		err := testCustomerSVC.AddCustomerCard(test.c)
		result := err == nil
		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v with an error of %v. ID=%d", test.expected, result, err, i+1)
		}
	}
}

func TestRemoveCustomerCard(t *testing.T) {
	tests := []struct {
		id       string
		expected bool
	}{
		{testCustomerCard.ID, true},
		{"nop", false},
	}

	for i, test := range tests {
		err := testCustomerSVC.RemoveCustomerCard(test.id)
		result := err == nil
		if result != test.expected {
			t.Errorf("Expected result to be %v, got %v with an error of %v. ID=%d", test.expected, result, err, i+1)
		}
	}
}

func TestCustomerResetPassword(t *testing.T) {
	tests := []struct {
		tid      int
		p        testResetPwdInput
		expected bool
	}{
		{1, testResetPwdInput{id: testCustomer.ID, password: testCustomerPassword}, true},
		{2, testResetPwdInput{id: "323213", password: testCustomerPassword}, false},
		{3, testResetPwdInput{id: testCustomer.ID, password: "nop"}, false},
	}

	for _, test := range tests {
		err := testCustomerSVC.ResetPassword(test.p.id, test.p.password, testUpdatedPassword)
		result := err == nil

		if result != test.expected {
			t.Errorf("Unexpected result expecting %v, got %v., with an error: %v, id is %d", test.expected, result, err, test.tid)
		}
	}
}
