package postgre

import (
	"strconv"
	"time"

	"gitlab.com/vanoapp/api"
	pg "gopkg.in/pg.v5"
)

// TransactionService is
type TransactionService struct {
	DB              *pg.DB
	CustomerService api.CustomerService
}

// CreateTransaction is
func (s *TransactionService) CreateTransaction(t *api.Transaction) error {
	t.New()
	if t.Card != nil {
		return s.createCardTransaction(t)
	}
	return s.createTokenTransaction(t)
}

func (s *TransactionService) createCardTransaction(t *api.Transaction) error {
	t.CardMethod = t.Card.Method
	t.PaymentType = "card"
	intLastFour, err := strconv.Atoi(t.Card.Number[len(t.Card.Number)-4:])
	if err != nil {
		return err
	}
	t.LastFour = intLastFour

	return s.DB.Insert(t)
}

func (s *TransactionService) createTokenTransaction(t *api.Transaction) error {
	t.PaymentType = "token"

	c, err := s.CustomerService.GetCustomerCardByToken(t.Token)
	if err != nil {
		return err
	}
	t.LastFour = c.LastFour
	t.CardMethod = c.Method

	return s.DB.Insert(t)
}

// ListProfessionalTransactions is
func (s *TransactionService) ListProfessionalTransactions(id string, fromDate int64) ([]*api.Transaction, error) {
	date := time.Unix(fromDate, 0)
	var t []*api.Transaction
	if err := s.DB.Model(&t).Where("professional_id = ?", id).Where("created_at > ?", date).Order("created_at").Select(); err != nil {
		return []*api.Transaction{}, err
	}

	return t, nil
}

// GetTransactionByID is
func (s *TransactionService) GetTransactionByID(id string) (*api.Transaction, error) {
	t := new(api.Transaction)
	if err := s.DB.Model(t).Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	return t, nil
}

// RefundTransaction is
func (s *TransactionService) RefundTransaction(id string) error {
	t, err := s.GetTransactionByID(id)
	if err != nil {
		return err
	}

	t.Refunded = true
	t.UpdatedAt = time.Now()

	if _, err := s.DB.Model(t).Column("refunded", "updated_at").Update(); err != nil {
		return err
	}

	return nil
}
