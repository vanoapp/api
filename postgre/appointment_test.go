package postgre

import (
	"testing"
	"time"

	"fmt"

	"gitlab.com/vanoapp/api"
)

func TestCreateAppointment(t *testing.T) {
	professional2 := createRandProfessional(testCompany.ID)

	services := []*api.Service{
		&api.Service{
			ID:     testService.ID,
			Length: 50,
		},
	}

	tests := []struct {
		a        *api.Appointment
		expected bool
	}{
		{&api.Appointment{ProfessionalID: testProfessional.ID, CustomerID: testCustomer.ID, Notes: "bring towel", Services: services, StartTime: time.Now().Add(10 * time.Hour).Unix(), EndTime: time.Now().Add(11 * time.Hour).Unix()}, true},
		{&api.Appointment{ProfessionalID: testProfessional.ID, CustomerID: testCustomer.ID, Notes: "bring towel", Services: services, StartTime: time.Now().Add(10 * time.Hour).Add(30 * time.Minute).Unix(), EndTime: time.Now().Add(12 * time.Hour).Unix()}, false},
		{&api.Appointment{ProfessionalID: testProfessional.ID, CustomerID: testCustomer.ID, Services: services, StartTime: time.Now().Add(12 * time.Hour).Unix(), EndTime: time.Now().Add(13 * time.Hour).Unix()}, true},
		{&api.Appointment{ProfessionalID: professional2.ID, CustomerID: testCustomer.ID, Notes: "bring towel", Services: services, StartTime: time.Now().Add(10 * time.Hour).Unix(), EndTime: time.Now().Add(11 * time.Hour).Unix()}, true},
		{&api.Appointment{ProfessionalID: testProfessional.ID, CustomerID: testCustomer.ID, Notes: "bring towel", Services: services, StartTime: time.Now().Add(10 * time.Hour).Unix(), EndTime: time.Now().Add(11 * time.Hour).Unix()}, false},
		{&api.Appointment{ProfessionalID: testProfessional.ID, CustomerID: testCustomer.ID, Notes: "bring towel", StartTime: time.Now().Add(16 * time.Hour).Unix(), EndTime: time.Now().Add(18 * time.Hour).Unix()}, false},
		{&api.Appointment{ProfessionalID: testProfessional.ID, Notes: "bring towel", Services: services, StartTime: time.Now().Add(10 * time.Hour).Unix(), EndTime: time.Now().Add(11 * time.Hour).Unix()}, false},
		{&api.Appointment{CustomerID: testCustomer.ID, Notes: "bring towel", Services: services, StartTime: time.Now().Add(10 * time.Hour).Unix(), EndTime: time.Now().Add(11 * time.Hour).Unix()}, false},
	}

	for i, test := range tests {
		err := testAppointmentSVC.CreateAppointment(test.a)
		result := err == nil
		if result != test.expected {
			t.Errorf("Unexpected result. expecting reuslt to be %v, got %v when id is %d, with an error: %v", test.expected, result, i+1, err)
		}

		if err == nil {
			switch {
			case test.a.Services == nil:
				t.Error("Expecting services to not be nil")
			}
		}
	}
}

func TestListProfessionalAppointments(t *testing.T) {
	start := time.Now().Truncate(24 * time.Hour).Unix()
	end := time.Now().Add(24 * time.Hour).Unix()

	tests := []struct {
		professionalID string
		expected       bool
	}{
		{testProfessional.ID, true},
	}

	for i, test := range tests {
		appointments, err := testAppointmentSVC.ListProfessionalAppointments(test.professionalID, start, end, 5)
		result := err == nil
		if result != test.expected {
			t.Errorf("Unexpected result. expecting reuslt to be %v, got %v when id is %d, with an error: %v", test.expected, result, i+1, err)
		}

		if err == nil {
			for _, a := range appointments {
				switch {
				case a.Services == nil:
					t.Error("Expecting services to not be nil")
				}
			}
		}
	}
}
func TestCheckAppointmentIsAvailable(t *testing.T) {
	tests := []struct {
		start    int64
		end      int64
		expected bool
	}{
		{testAppointment.StartTime, testAppointment.EndTime, false},
		{time.Unix(testAppointment.StartTime, 0).Add(10 * time.Minute).Unix(), time.Unix(testAppointment.EndTime, 0).Add(10 * time.Minute).Unix(), false},
		{time.Unix(testAppointment.StartTime, 0).Add(45 * time.Minute).Unix(), time.Unix(testAppointment.EndTime, 0).Add(65 * time.Minute).Unix(), true},
		{123456, 1234567, true},
	}

	for i, test := range tests {
		err := testAppointmentSVC.CheckAppointmentIsAvailable(testProfessional.ID, test.start, test.end)
		result := err == nil
		if result != test.expected {
			fmt.Println(test.start)
			fmt.Println(test.end)
			t.Errorf("Unexpected result. expecting reuslt to be %v, got %v when id is %d, with an error: %v", test.expected, result, i+1, err)
		}
	}
}
func TestUpdateAppointment(t *testing.T) {
	services := []*api.Service{
		&api.Service{
			ID:     testService.ID,
			Length: 50,
		},
		&api.Service{
			ID:     testService.ID,
			Length: 60,
		},
	}

	servicesOne := []*api.Service{
		&api.Service{
			ID:     testService.ID,
			Length: 60,
		},
	}

	tests := []struct {
		a        *api.Appointment
		expected bool
	}{
		{&api.Appointment{ID: testAppointment.ID, ProfessionalID: testProfessional.ID, CustomerID: testCustomer.ID, Notes: "updated towel", Services: services, StartTime: time.Now().Unix(), EndTime: time.Now().Unix()}, true},
		{&api.Appointment{ID: testAppointment.ID, ProfessionalID: testProfessional.ID, CustomerID: testCustomer.ID, Notes: "updated towel", Services: servicesOne, StartTime: time.Now().Unix(), EndTime: time.Now().Unix()}, true},
	}

	for i, test := range tests {
		err := testAppointmentSVC.UpdateAppointment(test.a)
		result := err == nil
		if result != test.expected {
			t.Errorf("Unexpected result. expecting reuslt to be %v, got %v when id is %d, with an error: %v", test.expected, result, i+1, err)
		}
	}
}
