package postgre

import (
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/vanoapp/api"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/pg.v5"
)

// CustomerService is
type CustomerService struct {
	DB            *pg.DB
	SearchService api.SearchService
}

// CreateCustomer is
func (s *CustomerService) CreateCustomer(c *api.CreateCustomer) (*api.Customer, error) {
	customer := &api.Customer{
		FirstName:   strings.ToLower(c.FirstName),
		LastName:    strings.ToLower(c.LastName),
		Email:       strings.ToLower(c.Email),
		PhoneNumber: c.PhoneNumber,
		SplashID:    c.SplashID,
		Password:    c.Password,
	}

	customer.New()
	if err := customer.HashPassword(); err != nil {
		return nil, err
	}

	if err := customer.ValidateCreate(); err != nil {
		return nil, err
	}

	if err := s.DB.Insert(customer); err != nil {
		return nil, err
	}
	if c.CompanyID != "" {
		if err := s.AddCustomerCompany(customer.ID, c.CompanyID); err != nil {
			return nil, err
		}
		var comps []*api.Company
		comp := &api.Company{
			ID: c.CompanyID,
		}
		comps = append(comps, comp)
		customer.Companies = comps
	}

	if c.ProfessionalID != "" {
		if err := s.AddProfessionalCustomer(customer.ID, c.ProfessionalID); err != nil {
			return nil, err
		}
	}

	go func() {
		searchCustomer := &api.SearchCustomer{
			ID:             customer.ID,
			CompanyID:      c.CompanyID,
			ProfessionalID: c.ProfessionalID,
			FirstName:      c.FirstName,
			LastName:       c.LastName,
			Email:          c.Email,
			PhoneNumber:    c.PhoneNumber,
		}
		if err := s.SearchService.AddCustomer(searchCustomer); err != nil {
			log.Println("error adding customer object", err)
		}
	}()

	return customer, nil
}

//AddProfessionalCustomer is
func (s *CustomerService) AddProfessionalCustomer(customerID, professionalID string) error {
	newRow := &api.CustomerProfessional{
		ID:             uuid.NewV4().String(),
		CustomerID:     customerID,
		ProfessionalID: professionalID,
	}
	return s.DB.Insert(newRow)
}

// AddCustomerCard is
func (s *CustomerService) AddCustomerCard(c *api.CustomerCard) error {
	if err := c.New(); err != nil {
		return err
	}

	return s.DB.Insert(c)
}

// GetCustomer is
func (s *CustomerService) GetCustomer(id string) (*api.Customer, error) {
	var c api.Customer
	if err := s.DB.Model(&c).Column("customer.*", "Companies.id").Where("id = ?", id).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	var n []*api.CustomerNote
	if err := s.DB.Model(&n).Where("customer_id = ?", id).Select(); err != nil {
		return nil, err
	}

	c.Notes = n

	var cc []*api.CustomerCard
	if err := s.DB.Model(&cc).Where("customer_id = ?", id).Select(); err != nil {
		return nil, err
	}

	c.Cards = cc

	return &c, nil
}

// CheckCustomerExists is
func (s *CustomerService) CheckCustomerExists(email, phoneNumber string) (*api.Customer, error) {
	var c api.Customer
	if err := s.DB.Model(&c).Column("customer.*", "Companies.id").Where("email = ?", email).WhereOr("phone_number = ?", phoneNumber).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	var n []*api.CustomerNote
	if err := s.DB.Model(&n).Where("customer_id = ?", c.ID).Select(); err != nil {
		return nil, err
	}

	c.Notes = n

	return &c, nil
}

// RemoveCustomerCard is
func (s *CustomerService) RemoveCustomerCard(id string) error {
	r, err := s.DB.Model(&api.CustomerCard{}).Where("id = ?", id).Delete()
	if err != nil {
		return err
	}

	if r.RowsAffected() == 0 {
		return api.ErrNotFound
	}

	return nil
}

// GetCustomerCards is
func (s *CustomerService) GetCustomerCards(customerID string) ([]*api.CustomerCard, error) {
	if _, err := s.GetCustomer(customerID); err != nil {
		return nil, err
	}

	var cards []*api.CustomerCard

	if err := s.DB.Model(&cards).Where("customer_id = ?", customerID).Select(); err != nil {
		return nil, err
	}

	return cards, nil
}

// GetCustomerQuery is
func (s *CustomerService) GetCustomerQuery(query string, params ...interface{}) (*api.Customer, error) {
	var c api.Customer

	if _, err := s.DB.QueryOne(&c, query, params...); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	return &c, nil
}

// ListCompanyCustomers is
func (s *CustomerService) ListCompanyCustomers(companyID string, page int) ([]*api.Customer, error) {
	fmt.Println("company id", companyID)
	offset := page * 30
	if page == 1 {
		offset = 0
	}
	limit := 30

	var c []*api.Customer
	if err := s.DB.Model(&c).Column("customer.*", "Companies.id", "Companies.name").Join("inner join company_customer cc on customer.id = cc.customer_id").Where("cc.company_id = ?", companyID).Offset(offset).Limit(limit).Order("first_name").Select(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	for _, customer := range c {
		var n []*api.CustomerNote
		if err := s.DB.Model(&n).Where("customer_id = ?", customer.ID).Select(); err != nil {
			return nil, err
		}

		customer.Notes = n

	}

	return c, nil
}

// ListProfessionalCustomers is
func (s *CustomerService) ListProfessionalCustomers(professionalID string, page int) ([]*api.Customer, error) {
	offset := page * 50
	if page == 1 {
		offset = 0
	}
	limit := 50

	p := new(api.Professional)
	if _, err := s.DB.QueryOne(p, "select * from professionals where id = ? limit 1", professionalID); err != nil {
		return nil, err
	}

	if p == nil {
		return nil, errors.New("professional id not found")
	}

	query := `
	SELECT "c".*
	FROM   customers c 
		   LEFT JOIN customer_professional cp 
				  ON c.id = cp.customer_id 
		   LEFT JOIN company_customer cc 
				  ON c.id = cc.customer_id 
	WHERE  ( cp.professional_id = ? ) 
			OR ( cc.company_id = ? ) 
	ORDER  BY "first_name" 
	OFFSET ?
	LIMIT  ?; 
	`

	var c []*api.Customer
	if _, err := s.DB.Query(&c, query, professionalID, p.CompanyID, offset, limit); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	for _, customer := range c {
		var n []*api.CustomerNote
		if err := s.DB.Model(&n).Where("customer_id = ?", customer.ID).Select(); err != nil {
			return nil, err
		}
		customer.Notes = n
	}

	return c, nil
}

// UpdateCustomer is
func (s *CustomerService) UpdateCustomer(c *api.Customer) error {
	c.Update()

	r, err := s.DB.Model(c).Column("updated_at", "first_name", "last_name", "phone_number", "email", "notification", "last_login", "ip_address").Update()
	if err != nil {
		return err
	}

	if r.RowsAffected() == 0 {
		return api.ErrNotFound
	}

	go func() {
		sc := &api.SearchCustomer{
			ID:          c.ID,
			FirstName:   c.FirstName,
			LastName:    c.LastName,
			Email:       c.Email,
			PhoneNumber: c.PhoneNumber,
		}
		if err := s.SearchService.UpdateCustomer(sc); err != nil {
			log.Println("[ERROR] updating customer with Algolia", err)
		}
	}()

	return nil
}

// AddCustomerNote is
func (s *CustomerService) AddCustomerNote(n *api.CustomerNote) error {
	n.New()

	return s.DB.Insert(n)
}

// RemoveCustomerNote is
func (s *CustomerService) RemoveCustomerNote(id string) error {
	r, err := s.DB.Model(&api.CustomerNote{}).Where("id = ?", id).Delete()
	if err != nil {
		return err
	}

	if r.RowsAffected() == 0 {
		return api.ErrNotFound
	}

	return nil
}

// UpdateCustomerNote is
func (s *CustomerService) UpdateCustomerNote(n *api.CustomerNote) error {
	n.UpdatedAt = time.Now()

	res, err := s.DB.Model(n).Column("value", "updated_at").Update()
	if err != nil {
		return err
	}

	if res.RowsAffected() == 0 {
		return api.ErrNotFound
	}

	return nil
}

// AddCustomerCompany is
func (s *CustomerService) AddCustomerCompany(customerID, companyID string) error {
	cc := &api.CompanyCustomer{
		ID:         uuid.NewV4().String(),
		CustomerID: customerID,
		CompanyID:  companyID,
	}

	go func() {
		if err := s.SearchService.AddCompanyCustomer(customerID, companyID); err != nil {
			log.Println("error adding customer compnay object", err)
		}
	}()

	return s.DB.Insert(cc)
}

// RemoveCustomerCompany is
func (s *CustomerService) RemoveCustomerCompany(customerID, companyID string) error {
	r, err := s.DB.Model(&api.CompanyCustomer{}).Where("customer_id = ?", customerID).Where("company_id = ?", companyID).Delete()
	if err != nil {
		return err
	}

	if r.RowsAffected() == 0 {
		return api.ErrNotFound
	}

	go func() {
		if err := s.SearchService.RemoveCompanyCustomer(customerID, companyID); err != nil {
			log.Println("error removing customer compnay object", err)
		}
	}()

	return nil
}

// ResetPassword will call the database, get the professional, check if the password hash matches the database hash, if succeeds then it will store the new password in the database
func (s *CustomerService) ResetPassword(id, password, newPassword string) error {
	c, err := s.GetCustomer(id)
	if err != nil {
		return err
	}

	if err := bcrypt.CompareHashAndPassword([]byte(c.Password), []byte(password)); err != nil {
		return api.ErrNotFound
	}

	p, err := bcrypt.GenerateFromPassword([]byte(newPassword), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	c.Password = string(p)
	c.UpdatedAt = time.Now()

	if _, err := s.DB.Model(c).Column("password", "updated_at").Update(); err != nil {
		return err
	}

	return nil
}

// SetAccount is
func (s *CustomerService) SetAccount(id, password string) error {
	c, err := s.GetCustomer(id)
	if err != nil {
		return err
	}

	p, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	c.Password = string(p)
	c.UpdatedAt = time.Now()

	if _, err := s.DB.Model(c).Column("password", "updated_at").Update(); err != nil {
		return err
	}

	return nil
}

//SetSplashID is
func (s *CustomerService) SetSplashID(id, splashID string) (*api.Customer, error) {
	c, err := s.GetCustomer(id)
	if err != nil {
		return nil, err
	}

	c.Update()
	c.SplashID = splashID

	r, err := s.DB.Model(c).Column("updated_at", "splash_id").Update()
	if err != nil {
		return nil, err
	}

	if r.RowsAffected() == 0 {
		return nil, api.ErrNotFound
	}

	return c, nil
}

//GetCustomerComapanies is
func (s *CustomerService) GetCustomerComapanies(id string) ([]string, error) {
	c, err := s.GetCustomer(id)
	if err != nil {
		return nil, err
	}

	var resp []string
	for _, comp := range c.Companies {
		resp = append(resp, comp.ID)
	}

	return resp, nil
}

//AddForgotPasswordRequest is
func (s *CustomerService) AddForgotPasswordRequest(id string) (*api.ForgotPasswordRequestTable, error) {
	req := new(api.ForgotPasswordRequestTable)
	req.ID = uuid.NewV4().String()
	req.CustomerID = id
	req.CreatedAt = time.Now()
	req.UpdatedAt = time.Now()
	req.TextHash = bson.NewObjectId().Hex()
	req.Hash = api.GenerateMD5Hash(req.TextHash)

	if err := s.DB.Insert(req); err != nil {
		return nil, err
	}

	return req, nil
}

//NoAuthResetRequest is
func (s *CustomerService) NoAuthResetRequest(plainHash, newPassword string) error {
	r := new(api.ForgotPasswordRequestTable)
	hashedText := api.GenerateMD5Hash(plainHash)
	if err := s.DB.Model(r).Where("hash = ?", hashedText).Where("used = ?", false).First(); err != nil {
		if err == pg.ErrNoRows {
			return api.ErrNotFound
		}
		return err
	}

	p, err := s.GetCustomer(r.CustomerID)
	if err != nil {
		return err
	}

	pass, err := bcrypt.GenerateFromPassword([]byte(newPassword), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	p.Password = string(pass)
	p.UpdatedAt = time.Now()

	if _, err := s.DB.Model(p).Column("password", "updated_at").Update(); err != nil {
		return err
	}

	r.Used = true
	r.UpdatedAt = time.Now()

	if _, err := s.DB.Model(r).Column("used", "updated_at").Update(); err != nil {
		return err
	}

	return nil
}

// GetCustomerCardByToken is
func (s *CustomerService) GetCustomerCardByToken(t string) (*api.CustomerCard, error) {
	c := new(api.CustomerCard)
	if err := s.DB.Model(c).Where("token = ?", t).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
		return nil, err
	}

	return c, nil
}

// AddServiceToCustomer is
func (s *CustomerService) AddServiceToCustomer(customerID, serviceID string, length int) (*api.CustomerServicesTable, error) {
	t := &api.CustomerServicesTable{
		ID:         uuid.NewV4().String(),
		CustomerID: customerID,
		ServiceID:  serviceID,
		Length:     length,
	}

	if err := s.DB.Insert(t); err != nil {
		return nil, err
	}

	return t, nil
}

// RemoveServiceFromCustomer is
func (s *CustomerService) RemoveServiceFromCustomer(id string) error {
	if _, err := s.DB.Model(&api.CustomerServicesTable{}).Where("id = ?", id).Delete(); err != nil {
		return err
	}
	return nil
}

// ListCustomerServices is
func (s *CustomerService) ListCustomerServices(customerID string) ([]*api.CustomerServicesTable, error) {
	var cs []*api.CustomerServicesTable
	if err := s.DB.Model(&cs).Where("customer_id = ?", customerID).Select(); err != nil {
		return nil, err
	}

	return cs, nil
}
