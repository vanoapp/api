package postgre

import (
	"gitlab.com/vanoapp/api"
	pg "gopkg.in/pg.v5"
)

// MerchantService is
type MerchantService struct {
	DB *pg.DB
}

// CreateMerchant is
func (s *MerchantService) CreateMerchant(m *api.Merchant) error {
	m.New()

	return s.DB.Insert(m)
}

// GetCompanyMerchant is
func (s *MerchantService) GetCompanyMerchant(companyID string) (*api.Merchant, error) {
	m := new(api.Merchant)

	if err := s.DB.Model(m).Where("company_id = ?", companyID).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
	}

	return m, nil
}

// GetProfessionalMerchant is
func (s *MerchantService) GetProfessionalMerchant(professionalID string) (*api.Merchant, error) {
	m := new(api.Merchant)

	if err := s.DB.Model(m).Where("professional_id = ?", professionalID).First(); err != nil {
		if err == pg.ErrNoRows {
			return nil, api.ErrNotFound
		}
	}

	return m, nil
}
