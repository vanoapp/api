package api

import (
	"encoding/json"
	"errors"
	"time"

	"strings"

	"regexp"

	uuid "github.com/satori/go.uuid"
)

// ProfessionalService is
type ProfessionalService interface {
	CreateProfessional(professional *Professional) error
	GetProfessional(id string) (*Professional, error)
	DeleteProfessional(id string) error
	GetProfessionalQuery(query string, params ...interface{}) (*Professional, error)
	UpdateProfessional(s *UpdateProfessional) (*Professional, error)
	ListProfessionals(limit int) ([]*Professional, error)
	ListCompanyProfessionals(companyID string) ([]*Professional, error)
	ResetPassword(id, password, newPassword string) error
	UpdateAuth(id, ipAddress string, lastLogin time.Time) error
	CheckProfessionalExists(email, phoneNumber string) (*Professional, error)
	ActivateProfessional(id, password string) error
	AddManagedProfessional(m *ManagedProfessional) (*ManagedProfessional, error)
	UpdateManagedProfessional(m *ManagedProfessional) (*ManagedProfessional, error)
	RemoveManagedProfessional(id string) error
	ListProfessionalManagedProfessionals(spID, status string) ([]*ManagedProfessional, error)
	ListProfessionalManageeInvitations(spID, status string) ([]*ManagedProfessional, error)
	AddForgotPasswordRequest(id string) (*ForgotPasswordRequestTable, error)
	NoAuthResetRequest(planHash, newPassword string) error
	UpdateProfessionalFeature(id, featureID string, enabled bool) error
	ClocIn(professionalID string) (*ClocIn, error)
	ClocOut(clocInID string) error
	ListProfessionalClocIns(id string) ([]*ClocIn, error)
}

// SanitizedProfessional is
type SanitizedProfessional struct {
	ID                 string                  `json:"id"`
	CompanyID          string                  `json:"company_id"`
	FirstName          string                  `json:"first_name"`
	LastName           string                  `json:"last_name"`
	Email              string                  `json:"email"`
	PhoneNumber        string                  `json:"phone_number"`
	Notification       *Notification           `json:"notification"`
	Permission         *ProfessionalPermission `json:"permission"`
	Onboarded          bool                    `json:"onboarded"`
	PasswordSet        bool                    `json:"password_set" sql:",notnull"`
	AdminGenerated     bool                    `json:"admin_generated" sql:",notnull"`
	Services           []*Service              `json:"services"`
	RequireCC          bool                    `json:"require_cc" sql:",notnull"`
	Features           []*Feature              `json:"features"`
	Independent        bool                    `json:"independent" sql:",notnull"`
	MerchantIntegrated bool                    `json:"merchant_integrated" sql:",notnull"`
}

// Professional is
type Professional struct {
	ID                 string                  `json:"id"`
	CompanyID          string                  `json:"company_id"`
	FirstName          string                  `json:"first_name"`
	LastName           string                  `json:"last_name"`
	Notification       *Notification           `json:"notification"`
	Permission         *ProfessionalPermission `json:"permission" sql:"permission"`
	Email              string                  `json:"email"`
	Password           string                  `json:"password"`
	PhoneNumber        string                  `json:"phone_number"`
	Services           []*Service              `json:"services"`
	IPAddress          string                  `json:"ip_address"`
	LastLogin          time.Time               `json:"last_login"`
	Onboarded          bool                    `json:"onboarded" sql:"onboarded,notnull"`
	PasswordSet        bool                    `json:"password_set" sql:",notnull"`
	AdminGenerated     bool                    `json:"admin_generated" sql:",notnull"`
	Features           []*Feature              `json:"features" pg:",many2many:feature_professional"`
	RequireCC          bool                    `json:"require_cc" sql:",notnull"`
	Independent        bool                    `json:"independent" sql:",notnull"`
	MerchantIntegrated bool                    `json:"merchant_integrated" sql:",notnull"`
	CreatedAt          time.Time               `json:"-"`
	UpdatedAt          time.Time               `json:"-"`
}

// New is
func (p *Professional) New() {
	p.ID = uuid.NewV4().String()
	p.CreatedAt = time.Now()
	p.UpdatedAt = time.Now()
	p.Notification = &Notification{
		SMS:   true,
		Email: true,
		Phone: false,
	}
}

// CreateInitialServices is
func (p *Professional) CreateInitialServices() error {
	if p.ID == "" {
		return errors.New("company id must be set to create services")
	}

	svc := []*Service{
		&Service{
			ID:             uuid.NewV4().String(),
			ProfessionalID: p.ID,
			Name:           "Haircut",
			Description:    "Cold-pressed 90's keytar meditation, tumeric plaid hella. Skateboard meditation selvage, wolf kale chips celiac paleo kickstarter. Af selfies franzen banh mi. Thundercats sartorial art party bushwick",
			Length:         30,
			Price:          2000,
			CreatedAt:      time.Now(),
			UpdatedAt:      time.Now(),
		},
		&Service{
			ID:             uuid.NewV4().String(),
			ProfessionalID: p.ID,
			Name:           "Nails",
			Description:    "Cold-pressed 90's keytar meditation, tumeric plaid hella. Skateboard meditation selvage, wolf kale chips celiac paleo kickstarter. Af selfies franzen banh mi. Thundercats sartorial art party bushwick",
			Length:         25,
			Price:          1299,
			CreatedAt:      time.Now(),
			UpdatedAt:      time.Now(),
		},
		&Service{
			ID:             uuid.NewV4().String(),
			ProfessionalID: p.ID,
			Name:           "Massage",
			Description:    "Cold-pressed 90's keytar meditation, tumeric plaid hella. Skateboard meditation selvage, wolf kale chips celiac paleo kickstarter. Af selfies franzen banh mi. Thundercats sartorial art party bushwick",
			Length:         60,
			Price:          7000,
			CreatedAt:      time.Now(),
			UpdatedAt:      time.Now(),
		},
		&Service{
			ID:             uuid.NewV4().String(),
			ProfessionalID: p.ID,
			Name:           "Wax",
			Description:    "Cold-pressed 90's keytar meditation, tumeric plaid hella. Skateboard meditation selvage, wolf kale chips celiac paleo kickstarter. Af selfies franzen banh mi. Thundercats sartorial art party bushwick",
			Length:         30,
			Price:          2000,
			CreatedAt:      time.Now(),
			UpdatedAt:      time.Now(),
		},
	}

	p.Services = svc

	return nil
}

// Sanitize is
func (p *Professional) Sanitize() {
	p.FirstName = strings.ToLower(p.FirstName)
	p.LastName = strings.ToLower(p.LastName)
	p.Email = strings.ToLower(p.Email)
	reg := regexp.MustCompile("[^0-9]+")
	p.PhoneNumber = reg.ReplaceAllString(p.PhoneNumber, "")
}

// ValidateCreate is
func (p *Professional) ValidateCreate() error {
	switch {
	case p.FirstName == "":
		return errors.New("first name is required")
	case p.LastName == "":
		return errors.New("last name is required")
	case p.CompanyID == "":
		return errors.New("company id is required")
	case p.Email == "":
		return errors.New("email address is required")
	case p.Password == "" && p.AdminGenerated == false:
		return errors.New("password is required")
	case p.PhoneNumber == "":
		return errors.New("phone number is required")
	case p.Permission == nil:
		return errors.New("permission is required")
	}

	ok, err := p.validatePermission()
	if err != nil {
		return err
	}
	if !ok {
		return errors.New("all permissions are required")
	}

	return nil
}

func (p *Professional) validatePermission() (bool, error) {
	bytPermission, err := json.Marshal(p.Permission)
	if err != nil {
		return false, err
	}

	strPermission := string(bytPermission)

	if !strings.Contains(strPermission, `"manage_appointments"`) {
		return false, nil
	}

	if !strings.Contains(strPermission, `"manage_professionals"`) {
		return false, nil
	}

	if !strings.Contains(strPermission, `"manage_analytics"`) {
		return false, nil
	}

	if !strings.Contains(strPermission, `"manage_company"`) {
		return false, nil
	}

	if !strings.Contains(strPermission, `"manage_customers"`) {
		return false, nil
	}

	if !strings.Contains(strPermission, `"manage_checkouts"`) {
		return false, nil
	}

	return true, nil
}

// ManagedProfessional is
type ManagedProfessional struct {
	ID         string                  `json:"id"`
	Manager    string                  `json:"manager"`
	Managee    string                  `json:"managee"`
	Permission *ProfessionalPermission `json:"permission"`
	Status     string                  `json:"status"`
	CreatedAt  time.Time               `json:"-"`
	UpdatedAt  time.Time               `json:"-"`
}

// ValidateCreate is
func (p *ManagedProfessional) ValidateCreate() error {
	switch {
	case p.Managee == "":
		return errors.New("managee is required")
	case p.Manager == "":
		return errors.New("manager is required")
	case p.Permission == nil:
		return errors.New("permission is required")
	}
	return nil
}

// ValidateUpdate is
func (p *ManagedProfessional) ValidateUpdate() error {
	switch {
	case p.Status == "":
		return errors.New("status is required")
	case p.Permission == nil:
		return errors.New("permission is required")
	}
	return nil
}

// ProfessionalPermission is
type ProfessionalPermission struct {
	ManageAnalytics     bool `json:"manage_analytics"`
	ManageAppointments  bool `json:"manage_appointments"`
	ManageProfessionals bool `json:"manage_professionals"`
	ManageCompany       bool `json:"manage_company"`
	ManageCustomers     bool `json:"manage_customers"`
	ManageCheckouts     bool `json:"manage_checkouts"`
}

func (pp *ProfessionalPermission) validate() {

}

// UpdateProfessional is
type UpdateProfessional struct {
	ID                 string                  `json:"id"`
	FirstName          string                  `json:"first_name"`
	LastName           string                  `json:"last_name"`
	Email              string                  `json:"email"`
	PhoneNumber        string                  `json:"phone_number"`
	Notification       *Notification           `json:"notification"`
	IPAddress          string                  `json:"ip_address"`
	Permission         *ProfessionalPermission `json:"permission" sql:"permission"`
	LastLogin          time.Time               `json:"last_login"`
	UpdatedAt          time.Time               `json:"-"`
	RequireCC          bool                    `json:"require_cc" sql:",notnull"`
	MerchantIntegrated bool                    `json:"merchant_integrated" sql:",notnull"`
}

// ValidateUpdate is
func (p *UpdateProfessional) ValidateUpdate() error {
	switch {
	case p.ID == "":
		return errors.New("id is required")
	case p.FirstName == "":
		return errors.New("first name is required")
	case p.LastName == "":
		return errors.New("last name is required")
	case p.Email == "":
		return errors.New("email address is required")
	case p.PhoneNumber == "":
		return errors.New("phone number is required")
	case p.Notification == nil:
		return errors.New("notification is required")
	case p.Notification == nil:
		return errors.New("notification is required")
	case p.Notification == nil:
		return errors.New("notification is required")
	case p.Notification == nil:
		return errors.New("notification is required")
	case p.Permission == nil:
		return errors.New("permission is required")
	}

	ok, err := p.validatePermission()
	if err != nil {
		return err
	}
	if !ok {
		return errors.New("all permissions are required")
	}

	return nil
}

func (p *UpdateProfessional) validatePermission() (bool, error) {
	bytPermission, err := json.Marshal(p.Permission)
	if err != nil {
		return false, err
	}

	strPermission := string(bytPermission)

	if !strings.Contains(strPermission, `"manage_appointments"`) {
		return false, nil
	}

	if !strings.Contains(strPermission, `"manage_company"`) {
		return false, nil
	}

	if !strings.Contains(strPermission, `"manage_professionals"`) {
		return false, nil
	}

	if !strings.Contains(strPermission, `"manage_analytics"`) {
		return false, nil
	}

	if !strings.Contains(strPermission, `"manage_customers"`) {
		return false, nil
	}

	if !strings.Contains(strPermission, `"manage_checkouts"`) {
		return false, nil
	}

	return true, nil
}

// Feature is
type Feature struct {
	ID          string    `json:"id"`
	Name        string    `json:"name"`
	CompanyWide bool      `json:"company_wide"`
	Description string    `json:"description"`
	Price       string    `json:"price"`
	SalePrice   string    `json:"sale_price"`
	Enabled     bool      `json:"enabled" sql:"-"`
	Order       int       `json:"order"`
	Benefits    string    `json:"benefits"`
	ForWho      string    `json:"for_who"`
	CreatedAt   time.Time `json:"-"`
	UpdatedAt   time.Time `json:"-"`
}

// FeatureProfessionalTable is
type FeatureProfessionalTable struct {
	TableName      struct{} `sql:"feature_professional" json:"-"`
	ID             string
	ProfessionalID string
	FeatureID      string
	Enabled        bool `sql:",notnull"`
}

//ClocIn is
type ClocIn struct {
	ID             string    `json:"id"`
	ProfessionalID string    `json:"professional_id"`
	ClocInTime     time.Time `json:"cloc_in_time"`
	ClocOutTime    time.Time `json:"cloc_out_time"`
	CreatedAt      time.Time `json:"created_at"`
	UpdatedAt      time.Time `json:"updated_at"`
}
