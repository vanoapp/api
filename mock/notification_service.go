package mock

import "gitlab.com/vanoapp/api"

// NotificationService is
type NotificationService struct {
}

// NotifyNewAppointment is
func (s *NotificationService) NotifyNewAppointment(a *api.Appointment) error {
	return nil
}

// NotifyCancelAppointment is
func (s *NotificationService) NotifyCancelAppointment(a *api.Appointment) error {
	return nil
}

// NotifyUpdateAppointment is
func (s *NotificationService) NotifyUpdateAppointment(a *api.Appointment) error {
	return nil
}

// NotifyNewAdminProfessional is
func (s *NotificationService) NotifyNewAdminProfessional(a *api.Professional) error {
	return nil
}

// NotifyManagedProfessionalInvite is
func (s *NotificationService) NotifyManagedProfessionalInvite(managee, manager *api.Professional) error {
	return nil
}

//NotifyProfessionalResetPassword is
func (s *NotificationService) NotifyProfessionalResetPassword(p *api.Professional, req *api.ForgotPasswordRequestTable) error {
	return nil
}

//NotifyCustomerResetPassword is
func (s *NotificationService) NotifyCustomerResetPassword(p *api.Customer, req *api.ForgotPasswordRequestTable) error {
	return nil
}
