package mock

import "gitlab.com/vanoapp/api"

var testReminderID = "correctid"

// ReminderService is
type ReminderService struct {
}

// CreateReminder is
func (s *ReminderService) CreateReminder(r *api.Reminder) error {
	r.New()

	return nil
}

// UpdateReminder is
func (s *ReminderService) UpdateReminder(r *api.UpdateReminder) (*api.Reminder, error) {
	if r.ID != testReminderID {
		return nil, api.ErrNotFound
	}
	return &api.Reminder{}, nil
}

// GetProfessionalReminders is
func (s *ReminderService) GetProfessionalReminders(professionalID string, limit int, completed bool) ([]*api.Reminder, error) {
	var r []*api.Reminder

	return r, nil
}

// GetReminder is
func (s *ReminderService) GetReminder(id string) (*api.Reminder, error) {
	r := new(api.Reminder)
	return r, nil
}

// DeleteReminder is
func (s *ReminderService) DeleteReminder(id string) error {
	return nil
}
