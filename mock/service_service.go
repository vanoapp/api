package mock

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/vanoapp/api"
)

// ServiceService is
type ServiceService struct {
}

// CreateService is
func (s *ServiceService) CreateService(p *api.Service) (*api.Service, error) {
	p.ID = uuid.NewV4().String()
	p.CreatedAt = time.Now()
	p.UpdatedAt = time.Now()

	return p, nil
}

// GetService is
func (s *ServiceService) GetService(id string) (*api.Service, error) {
	if id != "correctid" {
		return nil, api.ErrNotFound
	}
	var p api.Service
	return &p, nil
}

// UpdateService is
func (s *ServiceService) UpdateService(p *api.Service) error {
	if p.ID != "correctid" {
		return api.ErrNotFound
	}
	p.UpdatedAt = time.Now()
	return nil
}

// RemoveService is
func (s *ServiceService) RemoveService(id string) error {
	if id != "correctid" {
		return api.ErrNotFound
	}
	return nil
}
