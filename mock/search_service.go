package mock

import (
	"gitlab.com/vanoapp/api"
)

// SearchService is
type SearchService struct {
}

// AddCompany is
func (s *SearchService) AddCompany(c *api.SearchCompany) error {
	return nil
}

// UpdateCompany is
func (s *SearchService) UpdateCompany(c *api.SearchCompany) error {
	return nil
}

// AddProfessional is
func (s *SearchService) AddProfessional(c *api.SearchProfessional) error {
	return nil
}

// AddCustomer is
func (s *SearchService) AddCustomer(c *api.SearchCustomer) error {
	return nil
}

// UpdateCustomer is
func (s *SearchService) UpdateCustomer(c *api.SearchCustomer) error {
	return nil
}

//AddCompanyCustomer is
func (s *SearchService) AddCompanyCustomer(customerID, companyID string) error {
	return nil
}

//RemoveCompanyCustomer is
func (s *SearchService) RemoveCompanyCustomer(customerID, companyID string) error {
	return nil
}

//UpdateProfessionalServices is
func (s *SearchService) UpdateProfessionalServices(id string) error {
	return nil
}
