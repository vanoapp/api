package api

import (
	"errors"
	"fmt"
)

// Serivce Enpoint Requests

// CreateServiceRequest is
type CreateServiceRequest struct {
	ProfessionalID   string                       `json:"professional_id"`
	Name             string                       `json:"name"`
	Description      string                       `json:"description"`
	Price            int                          `json:"price"`
	Length           int                          `json:"length"`
	RequirePhoneCall bool                         `json:"require_phone_call"`
	Steps            []*CreateServiceaStepRequest `json:"steps"`
}

// Validate is
func (r *CreateServiceRequest) Validate() error {
	switch {
	case r.ProfessionalID == "":
		return errors.New("professional_id required")
	case r.Name == "":
		return errors.New("name required")
	case r.Price == 0:
		return errors.New("price required")
	case r.Length == 0:
		return errors.New("length required")
	case len(r.Steps) > 0:
		for _, s := range r.Steps {
			if err := s.Validate(); err != nil {
				return fmt.Errorf("service step error: %v", err)
			}
		}
	}
	return nil
}

// CreateServiceaStepRequest is
type CreateServiceaStepRequest struct {
	Name   string `json:"name"`
	Length int    `json:"length"`
	IsGap  bool   `json:"is_gap"`
}

// Validate is
func (r *CreateServiceaStepRequest) Validate() error {
	switch {
	case r.Name == "":
		return errors.New("name required")
	case r.Length == 0:
		return errors.New("length required")
	}
	return nil
}
